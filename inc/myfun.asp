<%
function getTime()
getTime=FormatDateTime(now(),2) & " " & FormatDateTime(now(),4)
end function
function chkPara(byVal para)
	if para="" or isnull(para) or isempty(para) then
		chkPara=false
		exit function
	end if
	dim regEx
	set regEx=new regexp
	regEx.global=true
	regEx.pattern="^\d+(,?\d+)*$"
	chkPara=regEx.test(para)
	set regEx=nothing
end function

sub insert_record(byVal table,byVal Parameters,byVal values)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	'conn.execute(sql)
	call setdbvalue(sql)
end sub

sub del_record(byVal table,byVal Conditions)'表名,条件,返回路径
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	sql="delete from "&table&" where "&Conditions
	call setdbvalue(sql)
end sub

sub update_record(byVal table,byVal Parameters,byVal Conditions)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="update "&table&" set "&Parameters&" where "&Conditions&""
	'conn.execute(sql)
	call setdbvalue(sql)
end sub

'********************************************
'子程序名：showInfo
'作  用：显示浮动信息提示
'参  数：title:浮动窗口的标题;page:返回的页面;content:结果内容
'page为空时隐藏浮动窗口
'返回值：无
'********************************************
sub showInfo(title,page,content)
if page="" then
page="history.back();"
else
page="location.href='"&page&"';"
end if
Response.Write("<script language='javascript'>alert('"&title&","&content&"');"&page&"</script>")
call closers(rs)
call closeconn()
response.End()
end sub
'判断是否已经登录

function isLogin()
dim user,pass,userid
dim tmparr
tmparr=getUserLoginInfo()
user=tmparr(1)
pass=tmparr(2)
userid=tmparr(0)
erase tmparr

if not IsValidEmail(user) or len(pass)<>32 or not chkrequest(userid) then 
	isLogin=false
	exit function
end if

set checkRs=server.CreateObject("adodb.recordset")
checkRs.open "select useremail from [user] where useremail='"&user&"' and password='"&pass&"' and id="&clng(userid)&"",conn,1,1
if checkRs.eof and checkRs.bof then
	call closers(checkrs)
	isLogin=false
	exit function
end if
call closers(checkrs)
isLogin=true
end function

'设置购物车产品数量、产品总金额cookies
function setCartCookies(spnum,spmoney)
call writecookies("","buyproductnum",spnum)
call writecookies("","buyproductmoney",clng(spmoney))
end function

function getDingdanState(flag)
select case flag
case 0
	getDingdanState="<font color=red>交易关闭</font>"
case 1
	getDingdanState="等待买家付款"
case 2
	getDingdanState="<font color=red>已经付款，等待卖家发货</font>"
case 3
	getDingdanState="<font color=green>已经发货，等待买家确认收货</font>"
case 4
	getDingdanState="交易完成"
case 5
	getDingdanState="<font color=red>申请退单</font>"
case 6
	getDingdanState="已经退单"
case 7
	getDingdanState="<font color=red>退单拒绝</font>"
case 33
	getDingdanState="付款未确认"
case 44
	getDingdanState="<font color=green>付款已确认,正在配货</font>"
end select
end function

'获取地址 根据ID（以逗号分隔）获取class表里面的name，以空格分隔
function getNameList(svalue)
dim tmparr,i
getNameList=""
if not chkpara(svalue) then exit function
sql="select id,name from class where id in("&svalue&") order by charindex(ltrim(id),'"&svalue&"')"
tmparr=getDBvalueList(sql)
if clng(tmparr(0,0))<>0 then
	for i=0 to ubound(tmparr,2)
		if getNameList="" then getNameList=tmparr(1,i) else getNameList=getNameList&"&nbsp;"&tmparr(1,i)
	next
end if
erase tmparr
end function

'获取地址 根据ID（以逗号分隔）获取class表里面的name，以空格分隔
function getcurNameList(svalue)
dim tmparr,i
getcurNameList=""
if not chkpara(svalue) then exit function
sql="select id,name from curclass where id in("&svalue&") order by charindex(ltrim(id),'"&svalue&"')"
tmparr=getDBvalueList(sql)
if clng(tmparr(0,0))<>0 then
	for i=0 to ubound(tmparr,2)
		if getcurNameList="" then getcurNameList=tmparr(1,i) else getcurNameList=getcurNameList&"&nbsp;"&tmparr(1,i)
	next
end if
erase tmparr
end function

function getOptionNameList(sparentid,sselectid,smode)
sselectid=clng(sselectid)
dim ttable,tmparr
dim i
ttable="class"
select case smode
case 1
ttable="curclass"
end select
sql="select id,[name] from "&ttable&" where parentid="&sparentid
tmparr=getdbvaluelist(sql)
getOptionNameList=""
if clng(tmparr(0,0))<>0 then
	for i=0 to ubound(tmparr,2)
		if clng(tmparr(0,i))=sselectid then
		getOptionNameList=getOptionNameList&"<option value="""&tmparr(0,i)&""" selected>"&tmparr(1,i)&"</option>"&vbcrlf
		else
		getOptionNameList=getOptionNameList&"<option value="""&tmparr(0,i)&""">"&tmparr(1,i)&"</option>"&vbcrlf
		end if
	next
end if
erase tmparr
end function

'根据发货ID获取发货的详细信息 0是否存在(非0为存在) 1联系人 2省份地区ID号 3街道 4邮编 5电话 6手机
'svalue 订单addressID的值 suserid:会员ID
function getfahuoaddress(svalue,suserid)
dim tmparr,i,tarea
svalue=clng(svalue):suserid=clng(suserid)
if svalue=0 then
	sql="select top 1 truename,area,address,postcode,tel,mobile from [user] where id="&suserid
else
	sql="select top 1 lxr,area,address,yb,tel,mobile from address where id="&svalue
end if
tmparr=getdbvalue(sql,6)
getfahuoaddress=tmparr
erase tmparr
end function

'获取用户登陆信息 返回数组 0用户id 1用户邮箱 2用户密码 3用户等级
function getUserLoginInfo()
dim tmparr(3)
tmparr(0)=readcookies("isLogin","id")
tmparr(1)=readcookies("isLogin","email")
tmparr(2)=readcookies("isLogin","pass")
tmparr(3)=readcookies("isLogin","type")
if not chkrequest(tmparr(0)) then tmparr(0)=0 else tmparr(0)=clng(tmparr(0))
if not chkrequest(tmparr(3)) then tmparr(3)=1 else tmparr(3)=cint(tmparr(3))
getUserLoginInfo=tmparr
erase tmparr
end function

'检查订单号是否有效
function chkDingdanNumber(snum)
if len(snum)=18 then chkDingdanNumber=true else chkDingdanNumber=false
end function

'检查是否满足库存 0满足库存 1库存不足 2订购的产品数量不能少于最小起订量 3订购的产品数量不能大于最大供应量
'返回数组 array(错误ID号,错误描叙)
function chkKucen(smin,smax,snum)
smin=clng(smin):smax=clng(smax):snum=clng(snum)
if smax<smin and smax<>-1 then 
	chkKucen=array(1,"当前产品库存不足")
	exit function
end if
if snum<smin then
	chkKucen=array(2,"订购的产品数量不能少于最小起订量")
	exit function
end if
if snum>smax and smax<>-1 then
	chkKucen=array(3,"订购的产品数量不能大于最大供应量")
	exit function
end if
chkKucen=array(0,"")
end function

'获取产品的价格
function getProductsPrice(sprice,pzheke,uflagid)
dim tmparr
dim tzheke
if not chkNumber(sprice) or not chkNumber(pzheke) then 
getProductsPrice=0
exit function
end if
pzheke=cdbl(pzheke):sprice=cdbl(sprice)
sql="select top 1 zheke from userflag where id="&uflagid
tmparr=getdbvalue(sql,1)
if tmparr(0)=0 then 
getProductsPrice=0 
else 
tzheke=cdbl(tmparr(1))
'如果当前会员打折 =1时是不打折 计算公式：(会员的折扣+产品的折扣)-1=该产品的总共折扣
if tzheke=1 then getProductsPrice=sprice else getProductsPrice=((tzheke+pzheke)-1)*sprice
end if
getProductsPrice=getprice(getProductsPrice,2)
erase tmparr
end function

'获取会员的等级信息 0是否成功 1名称 2起批金额 3升级所需积分 4升级所需消费总金额 5多少免运费(0为不免) 
function getUserFlagName(fid)
dim tmparr
sql="select top 1 [name],qipi,jifen,[money],yunfei from userflag where id="&fid
tmparr=getdbvalue(sql,5)
if tmparr(0)=0 then
getUserFlagName=array(0,"未知会员",10000,0,0,0)
else
getUserFlagName=tmparr
end if
erase tmparr
end function

'产生18位订单号
function MakeDingdanNumber()
dim ttime
dim tyear,tmonth,tday,thour,tminute,tsecond,trndNum
const tRndMax=4
ttime		= now()
tyear		= year(ttime)
tmonth		= month(ttime)
tday		= day(ttime)
thour		= hour(ttime)
tminute		= minute(ttime)
tsecond		= second(ttime)
if tmonth<10 then tmonth="0"&tmonth
if tday<10 then	tday="0"&tday
if thour<10 then thour="0"&thour
if tminute<10 then tminute="0"&tminute
if tsecond<10 then tsecond="0"&tsecond
'时间产生后共14位

'获取4位随机数字
trndNum=getRndNum(tRndMax)
MakeDingdanNumber=tyear&tmonth&tday&thour&tminute&tsecond&trndNum
end function

'获取缓存sfilepath模版文件名
function getCache(sfilepath)
dim tpath
tpath=webskinpath&"/"&sfilepath
getCache=ReadFromUTF(tpath,"utf-8")
end function


'写cookies 
'scookiesarr： 组名 如果为空则不使用 scookiesname：cookies名 svalue:值
function writecookies(byval scookiesarr,byval scookiesname,byval svalue)
if scookiesarr="" then
	'response.Cookies(scookiesname).domain=ssweburl  '由于有一些加盟商网址并没有设置，导致出现问题
	response.Cookies(scookiesname).path="/"
	Response.Cookies(scookiesname).Expires=dateadd("h",12,now())
	response.Cookies(scookiesname)=svalue
else
	'response.Cookies(scookiesarr).domain=ssweburl
	response.Cookies(scookiesarr).path="/"
	Response.Cookies(scookiesarr).Expires=dateadd("h",12,now())
	response.Cookies(scookiesarr)(scookiesname)=svalue
end if
end function

'都cookies
'scookiesarr： 组名 如果为空则不使用 scookiesname：cookies名
'返回值 如果没有则返回 ''
function readcookies(byval scookiesarr,byval scookiesname)
dim t
'response.Cookies(scookiesarr).domain=ssweburl
'response.Cookies(scookiesarr).path="/"
if scookiesarr="" then	
	t=request.Cookies(scookiesname)
else	
	t=request.Cookies(scookiesarr)(scookiesname)
end if
if chknull(t,1) then t="" else t=checkstr(t)
readcookies=t
end function

'---------------------数据处理---------------------------
'****************************************************
'函数名：getDbValue
'作  用：取一条记录
'参  数：Sql           传入参数 Sql语句
'       ArrayCount    传入参数 需要的字段的个数
'返回值:getDbValue      输出参数 为一个数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDbValueCommon(Sql,ArrayCount)
	ReDim NewArray(ArrayCount)
	Dim Recordcounts,Rs,i,Comm
	Set Comm = Server.CreateObject("ADODB.Command")
	With Comm
		Comm.ActiveConnection = CommonConn
		Comm.CommandType = 4
		Comm.CommandText = "ColumnGet"
		Comm.Prepared  = true
		Comm.Parameters.Append .CreateParameter("Return",2,4)
		Comm.Parameters.Append .CreateParameter("@Sql",200,1,1000,Sql)
		Set Rs = .Execute	
	End With
	Rs.Close
	Recordcounts=Comm(0)
	NewArray(0)=Recordcounts
	If Recordcounts=0 Then getDbValueCommon=NewArray:Exit Function
	Set Comm=Nothing
	Rs.Open
	For i = 0 to cint(ArrayCount-1)
		NewArray(i+1)=Rs(i)
	Next
	Rs.Close
	getDbValueCommon=NewArray
	Erase NewArray
	Sql =""
End Function


'****************************************************
'函数名：getDBValueList
'作  用：按指定sql语句取记录集
'参  数：Sql           传入参数 Sql语句
'返回值:getDBValueList      输出参数 为一个二维数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDBValueListCommon(Sql)

	Dim Comm,Rs
	Dim Recordcounts
	Dim ArrayRecord
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = CommonConn
	.CommandType = 4
	.CommandText = "Columnget"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@Sql",200,1,2000,Sql)'参数1为定义的变量 2为数据类型 3为数据宽度 4为值
	Set Rs=.Execute()
	End With
	Rs.Close 
	RecordCounts=Comm(0)
	Rs.Open	
	If RecordCounts=0 Then 
		Redim ArrayRecord(0,0):ArrayRecord(0,0)=0
	Else
		ArrayRecord=Rs.GetRows()
	end if
	Rs.Close
	getDBValueListCommon=ArrayRecord
	erase ArrayRecord
	Sql =""
End Function

'设置数据数据库 插入数据 修改数据 删除数据
'返回值：0为不成功 或者 没有影响行数 大于0的值，是当前操作影响的行数 如果是插入操作，刚返回插入后的ID号
Function setDBValueCommon(Sql)
	'On Error Resume Next
	Dim Comm,idnum
	dim tmparr
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = CommonConn
	.CommandType = 4
	.CommandText = "ColumnSet"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@Sql",200,1,2000,Sql)
	.Execute()
	End With
	if err then	
		setDBValueCommon=0
		err.clear		
	else
		setDBValueCommon = Comm(0)	
	end if
	Set Comm = Nothing	
	Sql=""
End Function

function closeCommonConn()
commonconn.close
set commonconn=nothing
end function

'获取页面关键字 并初始化
function getPagekeyword(spageName,spagePath)
dim tst,tmparr
tst="id>0"
if spageName="" and spagePath="" then getPagekeyword=array(0)
if spageName<>"" then tst=tst&" and [name]='"&spageName&"'"
if spagePath<>"" then tst=tst&" and [url]='"&spagePath&"'"
sql="select top 1 title,keyword,[desc] from pageKeyword where "&tst
tmparr=getdbvalue(sql,3)
if tmparr(0)<>0 then
	tmparr(1)=setcommon(tmparr(1))
	tmparr(2)=setcommon(tmparr(2))
	tmparr(3)=setcommon(tmparr(3))	
end if
getPagekeyword=tmparr:erase tmparr
end function

'网站浏览模式网站获取
function getUrl(sm)
dim tmparr(1,3)
webhtml=1
'产品详细页
tmparr(0,1)="/detail.asp?id=$1$"
tmparr(0,2)="/html/pro$1$.htm"
tmparr(0,3)="/pro$1$.html"
'新闻静态页
tmparr(1,1)="/news/detail.asp?id=$1$"
tmparr(1,2)="/newshtml/news$1$.htm"
tmparr(1,3)="/news$1$.htm"
getUrl=tmparr(sm,webhtml)
erase tmparr
end function

'
function getDomain()
          getDomain = REQUEST.SERVERVARIABLES("server_name")
end function

'获取同城巴士分站的城市名称
function getCityName()
dim d,arr,city 
d=REQUEST.SERVERVARIABLES("server_name")
arr=split(d,".")
city=arr(0)
erase arr
select case city	
	case "nb","ningbo":getCityName="宁波"
	case "tz","taizhou","tzh":getCityName="台州"
	case "hzh","hz","hangzhou":getCityName="杭州"
	case "nj","nanjing":getCityName="南京"
	case "szh","sz","suzhou":getCityName="苏州"
	case "hf","hefei":getCityName="合肥"
	case "fy","fuyang":getCityName="阜阳"
	case "jn","jinan":getCityName="济南"
	case "qd","qingdao":getCityName="青岛"
	case "nch","nc","nanchang":getCityName="南昌"
	case "fzh","fz","fuzhou":getCityName="福州"
	case "shh","sh","shanghai":getCityName="上海"
	case "nn","nanling":getCityName="南宁"
	case "gl","guilin":getCityName="桂林"
	case "chd","cd","chengdou":getCityName="成都"
	case "chq","cq","chongqing":getCityName="重庆"
	case "bj","beijing":getCityName="北京"
	case "shy","sy","shengyang":getCityName="沈阳"
	case "dl","dalian":getCityName="大连"
	case "herb","haerbin":getCityName="哈尔滨"
	case "ty","taiyuan":getCityName="聊城"
	case "zhzh","zz","zhengzhou":getCityName="郑州"
	case "wh","wuhan":getCityName="武汉"
	case "km","kunmeng":getCityName="昆明"
	case "xan","xian":getCityName="西安"
	case "lzh","lanzhou":getCityName="兰州"
	case "dt","datong":getCityName="大同"
	case "xm","xiameng":getCityName="厦门"
	case "kf","kaifeng":getCityName="开封"
	case "chsh","cs","changsha":getCityName="长沙"
	case "yw","yiwu":getCityName="义乌主站"	
	case "szs","shenzhen":getCityName="深圳"	
	case "yc","yichun":getCityName="宜春"	
	case else:getCityName="义乌主站"
end select
end function
%>

