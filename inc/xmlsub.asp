<%
Dim XmlobjDoc,XmlobjNode

'打开xml文档，加载到xml对象
Function ConnectXmlDoc(byVal Path)
	dim getyes
    Set XmlobjDoc=Server.CreateObject("Microsoft.XMLDOM")
    XmlobjDoc.Async=False
    ConnectXmlDoc=XmlobjDoc.Load(Server.MapPath(Path))
End Function

'释放xml文档对象
Function CloseXmlDoc()   
      Set XmlobjDoc=Nothing   
End Function

'保存xml所做的修改到xml文档
function SaveXmlDoc(byVal Path)

	XmlobjDoc.save(Server.MapPath(path))
end function

'创建结点列表集 Nodepath结点路径 格式如://root/list 如果为空则为整个xml文档
function createXmlobjNode(byVal Nodepath)
	if left(Nodepath,2)<>"//" then 
		if left(Nodepath,1)="/" then Nodepath="/"&Nodepath else Nodepath="//"&Nodepath
	end if
	set XmlobjNode=XmlobjDoc.selectNodes(Nodepath)
end function

'关闭结点列表集
function closeXmlobjNode()
	set XmlobjNode=Nothing
end function

'遍历指定父结点的所有节点
'返回遍历指定父结点的所有节点 如果ParentNode的值为空，就是返回整个xml文档的结点所有值
Function getNodeList(byVal ParentNode)
dim tNode,ti,tcontent,tobjNode
tcontent=""
'创建结点列表集
if left(ParentNode,2)<>"//" then 
	if left(ParentNode,1)="/" then ParentNode="/"&ParentNode else ParentNode="//"&ParentNode
end if
set tobjNode=XmlobjDoc.selectNodes(ParentNode)
'遍历父结点下的所有值
for each tNode in tobjNode
	'遍历当前结点的所有属性
	for ti=0 to tNode.Attributes.length-1
		tcontent=tcontent&tNode.Attributes(ti).name&"="&tNode.Attributes(ti).value&" "		
	next
	'html标记换行
	tcontent=tcontent&"<br />"
next
'关闭结点列表集
set tobjNode=nothing
getNodeList=tcontent
tNode="":ti="":tcontent=""
end function


'获取某个节点的元素值：节点对象、节点名称
Function GetNodeValue(byVal SNode,byVal EleName)
	ON Error Resume Next
	If SNode Is Nothing Then Exit Function
	Set TmpSub=SNode.SelectSingleNode(ELeName)
	If TmpSub Is Nothing Then
	Set TmpSub=SNode.Attributes.GetNamedItem(ELeName)
	If TmpSub Is Nothing Then Exit Function
	TmpRet=TmpSub.Value
	Else
	TmpRet=TmpSub.Text
	End If
	If Not IsNull(TmpRet) Then GetNodeValue=TmpRet
	Set TmpSub=Nothing
End Function

'获取节点所在位置：节点集、节点名称、节点值
'返回值：返回数值 如果获取失败刚返回-1
Function GetIndexByValue(byVal EleName,byVal EleValue)
	dim Tmpi,EValue
    For Tmpi=0 To XmlobjNode.Length-1
        EValue=GetNodeValue(XmlobjNode(Tmpi),EleName)
        If EValue=clng(EleValue) Then
            GetIndexByValue=Tmpi
            Exit Function
        End If
    Next
    GetIndexByValue=-1
End Function

'获取一个节点：EleName 节点属性名 EleValue节点属性值
'返回值：返回一个节点 如果获取失败刚返回nothing
Function GetNodeByValue(byVal EleName,byVal EleValue)
	dim tNode,Tvalue
    For each tNode in XmlobjNode		
        Tvalue=clng(GetNodeValue(tNode,EleName))
        If Tvalue=clng(EleValue) Then
            set GetNodeByValue=tNode
            Exit Function
        End If
    Next
   set GetNodeByValue=nothing
End Function

'添加节点或者属性
'nodePos 节点位置(一个节点对象)
'key 0节点1属性、有无内容、内容、在某个节点之前插入节点
'EleName节点名或者属性名(由key的值决定)
'IfTxt 是否有值 1为有值，其它数值为无值 
'Text 节点或属性的值
'TmpIdx 在某个节点之前插入新节点 如果为-1则为当前结点下添加子结点
'如果操作成功，则返回一个节点或者属性对象
Function CreateXmlNode(NodePos,byVal EleName,byVal Key,byVal IfTxt,byVal Text,byVal TmpIdx)
dim SubNode
    Select Case Key
        Case 0
            Set SubNode=XmlobjDoc.CreateElement(EleName)
            If TmpIdx > -1 Then    '在某个节点之前插入新节点
                Set CurNode=NodePos.InsertBefore(SubNode,NodePos.ChildNodes(TmpIdx))
                If IfTxt=1 Then CurNode.text=Text
                Set CreateXmlNode=CurNode
                Set CurNode=Nothing
            Else    '
                If IfTxt=1 Then SubNode.text=Text				
                NodePos.AppendChild SubNode
                Set CreateXmlNode=SubNode
            End If
            Set SubNode=Nothing
        Case 1
            Set AttNode=XmlobjDoc.CreateAttribute(EleName)
            If IfTxt=1 Then AttNode.text=Text
            NodePos.Attributes.SetNamedItem AttNode
            Set CreateXmlNode=AttNode
            Set AttNode=Nothing
        Case Else
           set CreateXmlNode=nothing
    End Select
End Function

'添加一组节点[相当于组合节点，一次只能插入一组对应节点]：父节点、XML内容、节点位置
Function JoinXmlNode(byVal NodePos,byVal XmlStr,byVal TmpIdx)
	dim RootNewNode  
    Set RootNewNode=XmlobjDoc.documentElement
    If TmpIdx > -1 Then
        NodePos.InsertBefore RootNewNode,NodePos.ChildNodes(TmpIdx)
    Else
        NodePos.AppendChild(RootNewNode)
    End If
    Set RootNewNode=Nothing
End Function

'编辑某个节点内容
'NodePos 操作的节点对象
'EleName 节点或属性的名称(由key决定)
'Text 修改后的值
'Key 0为节点,其它值为属性
Function EditXmlNode(byVal NodePos,byVal EleName,byVal Text,byVal Key)
    If Key=0 Then    '节点
        Set EleNode=NodePos.SelectSingleNode(EleName) 
        EleNode.text=Text
    Else    '属性
        Set EleNode=NodePos.Attributes.GetNamedItem(EleName)
        EleNode.value=Text
    End If
    Set EleNode=Nothing
End Function

'删除节点
'PNode 当前操作的节点
'EleName节点的名称或属性名称(由Key决定,当IfNode的值为非0时，EleName的值将无效)
'IfNode是否删除单个节点 0为单个结点 其它值为删除当前节点之内的所有结点
'Key 0为节点 其它值为属性(当IfNode的值为非0时,Key的值将无效)
Function DelXmlNode(byVal PNode,byVal EleName,byVal IfNode,byVal Key)
    If IfNode=0 Then
        If Key=0 Then    '节点
            Set EleNode=PNode.SelectSingleNode(EleName) 
            PNode.RemoveChild EleNode
            Set EleNode=Nothing
        Else    '属性
            PNode.Attributes.RemoveNamedItem(EleName)
        End If
    Else    '删除节点之内所有节点
        PNode.parentNode.removeChild(PNode)
    End If
End Function
%> 
