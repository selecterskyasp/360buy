<!--#include virtual="/inc/top.asp"-->
<!--#include file="mhelpsub.asp"-->
<%
id=request.QueryString("id")
if not chkrequest(id) then
	response.Write("error")
	response.End()
end if
sql="select top 1 * from mhelp where id="&id
set rs=server.CreateObject("adodb.recordset")
rs.open sql,conn,1,1
if rs.bof and rs.eof then
	alert "error","",1
end if
bidname=getcurNameList(rs("bid"))
sql="update mhelp set hits=hits+1 where id="&id
call setdbvalue(sql)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2009 www.intltj.com" />
<meta name="Keywords" content="Yiwu Tujin Co., Ltd." />
<meta name="Description" content="Yiwu Tujin Co., Ltd." />
<link rel="stylesheet" href="style/common.css" type="text/css" media="all" />
<title><%=rs("name")%>-后台帮助</title>
</head>
<body>

<!--header begin-->
<div id="head">
<div class="company">
<div class="HeadMenu">
<ul>
<li><h3><a href="./">首页</a></h3></li>
<li><h3><a href="help.asp">在线帮助</a></h3></li>
<li><h3><a href="logout.asp">退出后台</a></h3></li>
</ul>
</div>
</div>
</div>
<!--header end-->

<!--content begin-->
<div id="content">
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="help.asp">帮助首页</a> >> <a href="help.asp?id=<%=id%>"><%=bidname%></a> >> <strong><%=rs("name")%></strong>
</div>
<div id="ContentLeft">
<ul class="MenuBar">
<span class="title">在线帮助</span>
   <ul id="opt_1" class="help">
   <%=getmhelpClass()%>
   </ul>
</ul>
<ul class="MenuBar">
<span class="title">我要搜索</span>
   <ul class="help">
   <form action="help.asp" method="get">
 	<input type="text" name="keyword" style="width:80px" id="keyword" />&nbsp;<input type="submit" value="搜索帮助" />
   </form>
   </ul>
</ul>
</div>

<div id="ContentRight">
<div class="RightTitleBg">
<div class="RightTitle">
<h2>详细信息</h2>
</div>
</div>
<div id="RightContent">
<div class="ColumnContent">
<h2 align="center"><%=rs("name")%></h2>
<hr />
<div style="text-align:center">发表时间：<%=rs("createdon")%> 发布人：<%=rs("adduser")%> 浏览次数：<%=rs("hits")%></div><br /><br />
  <%=replace_t(rs("content"))%>
</div>
</div>

</div>
</div>
<!--content end-->
 <%rs.close
 set rs=nothing
 call closeconn()%>

</body>
</html>