﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(15,qxarr)
if tqxarr(1)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加与修改</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="<%=webvirtual%>/jquery/css/cmxform.css" />
<style type="text/css">
.warning { color: red; }
</style>
<script src="../../js/jquery.js" type="text/javascript"></script>
<script src="../../jquery/lib/jquery.form.js" type="text/javascript"></script>
<script src="../../js/function.js" language="javascript"></script>
<script type="text/javascript" src="../../ajaxupload/AnPlus.js"></script>
<script type="text/javascript" src="../../ajaxupload/AjaxUploader.js"></script>
<script type="text/javascript" src="../../js/extend.js"></script>
<script type="text/javascript">
function loadsortpath() {
		var selects1 = new linkSelect({
		el: 'sortpath',
	url: '/xml/class.xml',
			root: 'list1',
			field: 'list',
			text: 'Text',
			value: 'ID'
			});		
		}
function _sendData(f,act,re,h,obj)
{
	
document.getElementById("content").value=getEditorHTMLContents("content");
	return cjx.sendData(f,act,re,h,obj);	
}
    
   
// 获取编辑器中HTML内容   
function getEditorHTMLContents(EditorName) {  
    var oEditor = FCKeditorAPI.GetInstance(EditorName);   
    return(oEditor.GetXHTML(true));   
}   
   
// 获取编辑器中文字内容   
function getEditorTextContents(EditorName) {   
    var oEditor = FCKeditorAPI.GetInstance(EditorName);   
    return(oEditor.EditorDocument.body.innerText);   
}   
   
// 设置编辑器中内容   
function SetEditorContents(EditorName, ContentStr) {   
    var oEditor = FCKeditorAPI.GetInstance(EditorName) ;   
    oEditor.SetHTML(ContentStr) ;   
}  

</script>
<script language="javascript">
function uppic(frmname) {
window.open("uppic.asp?fuptype=pic&frmname="+frmname,"blank_","scrollbars=yes,resizable=no,width=650,height=450");
}
function addmorebigpic(){
	if(document.getElementById('spbigpic2').style.display=='none')
		document.getElementById('spbigpic2').style.display='';
	else if(document.getElementById('spbigpic3').style.display=='none')
		document.getElementById('spbigpic3').style.display='';
	else if(document.getElementById('spbigpic4').style.display=='none')		
		document.getElementById('spbigpic4').style.display='';		
	else if(document.getElementById('spbigpic5').style.display=='none')
		document.getElementById('spbigpic5').style.display='';
	else
		alert('无法再添加');	
}
function setMemo(){
	var m=document.getElementById("ms");
	var jl=document.getElementById("jl").value;
	if(m.value.length<2){
		m.value="普通会员价格:"+document.getElementById("price").value+" 元/"+jl+" 市场价格:"+document.getElementById("price1").value+" 元/"+jl+" 产生积分:"+document.getElementById("jf").value+" 点.本产品"+document.getElementById("min").value+jl+"起批";
		if(document.getElementById("ks").value.length>1)
			m.value+=" 规格:"+document.getElementById("ks").value;
		if(document.getElementById("zd").value.length>1)
			m.value+=" 材质:"+document.getElementById("zd").value;
		if(document.getElementById("tuai").value.length>1)
			m.value+=" 外箱尺寸:"+document.getElementById("tuai").value;
		if(document.getElementById("fg").value.length>1)
			m.value+=" 商品重量:"+document.getElementById("fg").value;
		if(document.getElementById("serice").value.length>1)
			m.value+=" 体积:"+document.getElementById("serice").value;
		if(document.getElementById("baozhuang").value.length>1)
			m.value+=" 包装:"+document.getElementById("baozhuang").value;					
		m.value+=" 装箱数:"+document.getElementById("zhuangxiao").value;	
		m.value+=" 推荐度:"+document.getElementById("p_key1").value+"个星";	
	}
	
}
</script>
<style type="text/css">
<!--
.STYLE1 {color: #FF0000}
-->
</style>
</head>

<body>
<!--#include file="../../fckeditor/fckeditor.asp"-->
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > <a href="list.asp">商品管理</a> > 商品添加与修改</td>
  </tr>
</table>
<%
dim action,sortpath,title,keywords,smallpic,bigpic1,bigpic2,bigpic3,bigpic4,bigpic5,bid,ljxs,content,ms,sxjf,price1,price2,zheke,ks,zd,tuai,fg,serice,hh,kc,adduser,id,p_key1,p_key,jl,zhuangxiao,min,max,pprice
id=request.querystring("id")
action=request.QueryString("act")
if action<>"add" and action<>"edit" then alert "参数错误","",1
if action="edit" and not chkrequest(id) then alert "ID号丢失","",1
dim tarr,i
if not chkrequest(id) then 
	title		= ""	'名称
	keywords	= ""	'关键字
	smallpic	= ""	'小图
	bigpic1		= ""	'大图
	bigpic2		= ""	'大图
	bigpic3		= ""	'大图
	bigpic4		= ""	'大图
	bigpic5		= ""	'大图
	bid			= 0		'网站栏目	
	content		= "&nbsp;"	'详细说明
	ms			= ""	'商品描述
	sxjf		= 0		'所属积分	
	price1		= ""		'市场价格
	price2		= ""		'成本价	
	zheke		= 1		'折扣
	ks			= ""	'款式(尺寸)
	zd			= ""	'主要质地(材质)
	tuai		= ""	'图案(装箱规格)
	fg			= 1	'风格(毛净重)
	serice		= ""	'适合季节(体积)
	hh			= ""	'货号
	kc		 	= 0		'库存
	ljxs		= 0		'累计销售
	p_key1	 	= 1 	' 推荐度
	p_key		= 0		'推荐
	jl			= ""	'产品计量单位
	zhuangxiao	= 1	'产品装箱数
	min			= 0
	max			= 0
	color		= 0	
	size		= 0
	baozhuang	= ""	'包装
	key1		= 0
	flag		= 1	
	pprice		= 0
	lyid		= 0
	lyurl		= ""
else
	sql="select top 1 [name],keywords,smallpic,bigpic1,bigpic2,bigpic3,bigpic4,bigpic5,bid,sortpath,content,ms,sxjf,price1,price2,ks, zd,tuai,fg,serice,hh,createdon,ljxs,P_key,p_key1,jl,zhuangxiao,zheke,min,max,color,size,baozhuang,key1,flag,jf,key2,price,lyid,lyurl from products where id="&id
	set rs=server.CreateObject("adodb.recordset")
	rs.open sql,conn,1,1
	if rs.bof and rs.eof then
			call showmsg("找不到此产品")		
	end if
	title		= rs("name")		'名称
	keywords	= rs("keywords")		'关键字
	smallpic	= rs("smallpic")		'小图
	bigpic1		= rs("bigpic1")		'大图
	bigpic2		= rs("bigpic2")		'大图
	bigpic3		= rs("bigpic3")		'大图
	bigpic4		= rs("bigpic4")		'大图
	bigpic5		= rs("bigpic5")		'大图
	bid			= rs("bid")		'网站整体分类（批发分类）
	sortpath	= rs("sortpath")
	content		= replace_t(rs("content"))		'详细说明
	ms			= rs("ms")		'商品描述
	sxjf		= clng(rs("sxjf"))	'所属积分
	price1		= getprice(rs("price1"),2)		'市场价格	
	price2		= getprice(rs("price2"),2)		'成本价格
	ks			= rs("ks")		'款式(尺寸)
	zd			= rs("zd")		'主要质地(材质)
	tuai		= rs("tuai")		'图案(装箱规格)
	fg			= getprice(rs("fg"),4)		'风格(毛净重)
	serice		= rs("serice")		'适合季节(体积)
	hh			= rs("hh")		'货号	
	ljxs		= rs("ljxs")		'累计销售
	p_key		= rs("P_key")		'推荐
	p_key1	 	= rs("p_key1") 		'推荐度	
	jl			= rs("jl")		'产品计量
	zhuangxiao	= rs("zhuangxiao")		'产品装箱数
	zheke		= getprice(rs("zheke"),2)	'折扣
	min			= rs("min")		'起批
	max			= rs("max")		'最大供应
	color 		= rs("color")		'颜色
	size		= rs("size")		'尺寸
	baozhuang	= rs("baozhuang")		'包装
	key1		= rs("key1")		'是否新品
	flag		= rs("flag")		'审核	
	jf			= clng(rs("jf"))		'产生积分	
	key2		= rs("key2")		'是否积分产品	
	pprice		= getprice(rs("price"),2)		'产品价格		
	lyid		= rs("lyid")		'来源ID
	lyurl		= rs("lyurl")		'来源ID
	rs.close
	set rs=nothing
end if 
if not chkrequest(lyid) then lyid=0 else lyid=cint(lyid)
%>
<form action="" class="cmxform"  name="form1" id="form1" method="post">
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
	<input type="hidden" name="action" id="action"  value="<%=action%>">
	<%if action="edit" then %>
	<input type="hidden" name="id" id="id" value="<%=id%>">
    <input type="hidden" name="sortpath2" id="sortpath2" value="<%=sortpath2%>">
    <input type="hidden" name="page" id="page" value="<%=page%>">
	<%end if%>
	<tr>
    <td class="tbLabel">商品类别:</td>
    <td class="tbMsg" colspan="2" align="left">					
				<div id="sortpath"></div>
				<%if not chkNull(sortpath,1) then
				dim st,sortnamearr,sortname
				tarr=split(sortpath,",")
				st="当前你选择的分类："
				for i=0 to ubound(tarr)
					sortnamearr=getDBvalue("select top 1 [name] from class where id="&tarr(i),1)
					if sortnamearr(0)>0 then 
						sortname=sortnamearr(1)
						st=st&" &gt; "&sortname
					else
						sortpath=""
						st="产品分类不正确，请重新选择"
						exit for
					end if
				next
				if i>=ubound(tarr) then st=st&"(注：留空为不修改原来的分类)"
				response.Write(st)
				erase tarr
				erase sortnamearr
				%>
				<input type="hidden" name="sortpathold" value="<%=sortpath%>" />
			<%end if%>	 </td>
  </tr>
  <script language="javascript">
  loadsortpath();
  </script>
  <tr>
    <td class="tbLabel">商品标题:</td>
    <td class="tbMsg"><input name="title" type="text" value="<%=title%>" class="required" minlength="2" maxlength="100" title="商品标题为2-100字!" size="50" onfocus="this.select()"/>
      <!--计量单位:
      <input name="jl" id="jl" type="text" value="<%=jl%>" class="required" minlength="1" maxlength="10" title="产品计量单位为1-10字!" onfocus="this.select();" style="width:25px;"/>
      -->产品编号:<input name="hh" type="text"  class="required" id="hh" style="width:70px;" title="商品货号为2-50字!" onfocus="this.select();" value="<%=hh%>" size="30" maxlength="50" minlength="2"/>
      <span class="STYLE1">*</span></td>
  </tr>
   <tr>
    <td class="tbLabel">商品小图:</td>
    <td class="tbMsg"><input name="smallpic" id="smallpic" type="text" value="<%=smallpic%>" class="required" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50"/><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('smallpic',this);">
      <span class="STYLE1">*</span>(推荐小图尺寸:266*166)
	  <div style="color:#FF0000; display:none" id="smallpicerrshow"></div>	  </td>
  </tr>
  <tr>
    <td class="tbLabel">商品大图1:</td>
    <td class="tbMsg"><input name="bigpic1" id="bigpic1" type="text" value="<%=bigpic1%>" class="required" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50"/><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('bigpic1',this);">  
    <a href="javascript:;" onclick="addmorebigpic();">添加多个大图</a>(推荐大图尺寸:400*400)
	 <div style="color:#FF0000; display:none" id="bigpic1errshow"></div></td>
  </tr>
  <tr id="spbigpic2" <%if bigpic2="" then response.Write("style=""display:none""")%>>
    <td class="tbLabel">商品大图2:</td>
    <td class="tbMsg"><input name="bigpic2" id="bigpic2" type="text" value="<%=bigpic2%>" class="{required:false,accept:'jpg|bmp|png|jpeg|gif'}" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50" /><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('bigpic2',this);" > 
     <a href="javascript:;" onclick="document.getElementById('bigpic2').value='';document.getElementById('spbigpic2').style.display='none';">移除</a>
	 <div style="color:#FF0000; display:none" id="bigpic2errshow"></div></td>
  </tr>
  <tr id="spbigpic3" <%if bigpic3="" then response.Write("style=""display:none""")%>>
    <td class="tbLabel">商品大图3:</td>
    <td class="tbMsg"><input name="bigpic3" id="bigpic3" type="text" value="<%=bigpic3%>" class="accept:'jpg|bmp|png|jpeg|gif'" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50" /><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('bigpic3',this);"> 
     <a href="javascript:;" onclick="document.getElementById('bigpic3').value='';document.getElementById('spbigpic3').style.display='none';">移除</a>
	 <div style="color:#FF0000; display:none" id="bigpic3errshow"></div></td>
  </tr>
  <tr id="spbigpic4" <%if bigpic4="" then response.Write("style=""display:none""")%>>
    <td class="tbLabel">商品大图4:</td>
    <td class="tbMsg"><input name="bigpic4" id="bigpic4" type="text" value="<%=bigpic4%>" class="accept:'jpg|bmp|png|jpeg|gif'" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50" /><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('bigpic4',this);"> 
     <a href="javascript:;" onclick="document.getElementById('bigpic4').value='';document.getElementById('spbigpic4').style.display='none';">移除</a>
	 <div style="color:#FF0000; display:none" id="bigpic4errshow"></div></td>
  </tr>
   <tr id="spbigpic5" <%if bigpic5="" then response.Write("style=""display:none""")%>>
    <td class="tbLabel">商品大图5:</td>
    <td class="tbMsg"><input name="bigpic5" id="bigpic5" type="text" value="<%=bigpic5%>" class="accept:'jpg|bmp|png|jpeg|gif'" title="请添加jpg|bmp|png|jpeg|gif 格式!" size="50" /><INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('bigpic5',this);"> 
    <a href="javascript:;" onclick="document.getElementById('bigpic5').value='';document.getElementById('spbigpic5').style.display='none';">移除</a>
	<div style="color:#FF0000; display:none" id="bigpic5errshow"></div></td>
  </tr>  
	<div id="uploadContenter" style="background-color:#eeeeee;position:absolute;border:1px #555555 solid;padding:3px;"></div>
<iframe style="display:none;" name="AnUploader"></iframe>
<script type="text/javascript">
	/*======================================
	下面语句使上传控件显示在上面ID为uploadContenter的Div标签
	提交URL为upload.asp保存目录为upload
	表单提交到上面name属性为AnUploader的iframe里面；
	========================================*/
	
	var AjaxUp=new AjaxProcesser("uploadContenter");
	
	//设置提交到的iframe名称
	AjaxUp.target="AnUploader";  
	
	//上传处理页面,尽量不要更改
	AjaxUp.url="<%=webvirtual%>/ajaxupload/manageupload.asp"; 
	
	//保存目录
	AjaxUp.savePath="/uploadpic";
	
	var contenter=document.getElementById("uploadContenter");
	contenter.style.display="none"; //隐藏容器
	
	function showUploader(objID,srcElement){
		AjaxUp.reset();  //重置上传控件
		var errID=objID+"errshow";
		contenter.style.display="block"; //显示容器
		var ps=_.abs(srcElement);//作用--获取指定标签的绝对坐标,目的是为了把上传控件定位到按钮下面
		contenter.style.top=(ps.y + 30) + "px";  
		contenter.style.left=(ps.x-180) + "px";
		//上传成功时要执行的程序
		AjaxUp.succeed=function(files){
		    var fujian=document.getElementById(objID);
			fujian.value=AjaxUp.savePath + "/" + files[0].name;  //因为上传控件就只上传一个 文件，这里索引是0
			contenter.style.display="none";			
			document.getElementById(errID).style.display="none";
		}
		//上传失败时要执行的程序
		AjaxUp.faild=function(msg){document.getElementById(errID).innerText="文件上传失败，原因："+msg;contenter.style.display="none";document.getElementById(errID).style.display="block"}
	}
</script>
<div style="color:#FF0000; display:none" id="sperr"></div>
  <tr>
    <td class="tbLabel">商品关键字:</td>
    <td class="tbMsg"><input name="keywords" type="text" class="required" id="keywords" title="关键字为2-100字!" onfocus="this.select();" value="<%=keywords%>" size="60" maxlength="100" minlength="2"/>
      <span class="STYLE1">*(多个以,分隔 重要！)</span></td>
  </tr>  
   <tr>
    <td class="tbLabel">产品价格:</td>
    <td class="tbMsg">
     <input name="price" type="text" class="required number" id="price" title="产品价格必须为非零数字!" onfocus="this.select();" value="<%=pprice%>" style="width:80px;"/>
     元 原价:
      <input name="price1" type="text" class="required number" id="price1" title="市场价必须为非零数字!" onfocus="this.select();" value="<%=price1%>" style="width:80px;"/>
      元
<!--&nbsp;成本价:
      <input name="price2" type="text" class="required number" id="price2" title="成本价必须为非零数字!" onfocus="this.select();" value="<%=price2%>" style="width:80px;"/>
      元--></td>
  </tr>
   <!--<tr>
    <td class="tbLabel">产生积分:</td>
    <td class="tbMsg">
      <input name="jf" type="text" class="required digits" id="jf" title="产生积分必须为非零整数!" onfocus="this.select();" value="<%=jf%>" style="width:80px;"/>
分
&nbsp; 折扣:
<input name="zheke" type="text" class="required digits" id="zheke" title="必须为非零数字!" onfocus="this.select();" value="<%=zheke%>" style="width:80px;"/>
折<span class="red">*</span>&nbsp; 累计销售:
<input name="ljxs" type="text" class="required digits" id="ljxs" title="累计销售为非零数字!" onfocus="this.select();" value="<%=ljxs%>" style="width:80px;"/>    </td>
  </tr>-->
  
  <tr>
    <td class="tbLabel"><!--款式-->产品产地:</td>
    <td class="tbMsg"><input name="ks" id="ks" type="text" value="<%=ks%>" onfocus="this.select();" />
      <span class="tbLabel"> <!--材质-->电话:
      <input name="zd" id="zd" type="text" value="<%=zd%>"  onfocus="this.select();"/>
      <!--外箱尺寸-->地址:
      <input name="tuai" id="tuai" type="text" value="<%=tuai%>"  onfocus="this.select();"/>
      </span></td>
  </tr>
  <tr>
    <td class="tbLabel"><!--图案-->
     <!-- 产品重量:-->交通:</td>
    <td class="tbMsg">
      <!--<input name="fg" type="text" id="fg" value="<%=fg%>" style="width:80px"  onfocus="this.select();"/>
      <span class="red">*</span>
      （单位：千克）  -->
     报名数:
      <input name="serice" id="serice" type="text" value="<%=serice%>"  onfocus="this.select();"/>
     <!--包装-->
      <input name="baozhuang" id="baozhuang" type="text" style="width:300px" value="<%=baozhuang%>"  onfocus="this.select();"/>     </td>
  </tr> 
  <tr>
    <td class="tbLabel">上牌时间:</td>
    <td class="tbMsg"><input name="min" type="text" class="required number" id="min" title="上牌时间年份为非零数字!" onfocus="this.select();" value="<%=min%>" style="width:80px;"/>年 <input name="max" type="text" class="required number" id="max" title="上牌时间月份为非零数字" onfocus="this.select();" value="<%=max%>" style="width:80px;"/> 月
        </td>
  </tr>
 <!-- <tr>
    <td class="tbLabel">起订数:</td>
    <td class="tbMsg"><input name="min" type="text" class="required digits" id="min" title="起订数为非零数字!" onfocus="this.select();" value="<%=min%>" style="width:80px;"/>
        <span class="red">*</span>
      库存:
      <input name="max" type="text" class="required number" id="max" title="起订数为数字 当值为-1时为无限库存!" onfocus="this.select();" value="<%=max%>" style="width:80px;"/>
      <span class="red">*</span>
      装箱数:
      <input name="zhuangxiao" type="text" id="zhuangxiao"  style="width:80px;" onfocus="this.select();" value="<%=zhuangxiao%>" size="60"/>
      （必须为数字 多少个/箱）<span class="red">*</span>      </td>
  </tr>   -->
  <tr>
    <td class="tbLabel">推荐度:</td>
    <td class="tbMsg">
	<input type="radio" name="p_key1" id="p_key1" value="1" <%if cint(p_key1)=1 then response.write "checked"%>/> <img src="/images/xin1.gif" /> 
	<input type="radio" name="p_key1" id="p_key2" value="2" <%if cint(p_key1)=2 then response.write "checked"%>/> <img src="/images/xin2.gif" /> 
	<input type="radio" name="p_key1" id="p_key3" value="3" <%if cint(p_key1)=3 then response.write "checked"%>/> <img src="/images/xin3.gif" /> 
	<input type="radio" name="p_key1" id="p_key4" value="4" <%if cint(p_key1)=4 then response.write "checked"%>/> <img src="/images/xin4.gif" /> 
	<input type="radio" name="p_key1" id="p_key5" value="5" <%if cint(p_key1)=5 then response.write "checked"%>/> <img src="/images/xin5.gif" />	</td>
  </tr> 
   
  
  <tr>
    <td class="tbLabel">商品描述:</td>
    <td class="tbMsg">
	<textarea name="ms" id="ms" cols="80" rows="2" class="required" minlength="10" title="请输入10到150个字的商品描述!" onfocus="setMemo()"><%=ms%></textarea>
	  <span class="STYLE1">*</span>（10-150字）	  </td>
  </tr>
  <tr>
    <td class="tbLabel">商品内容:<span class="tbMsg"><span class="STYLE1">*</span></span></td>
    <td class="tbMsg">
	<%
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath =webvirtual&"/fckeditor/"
	oFCKeditor.Value	= content
	oFCKeditor.width	= "100%"
	oFCKeditor.height	= "350"	
	'oFCKeditor.ToolbarSet	= "Basic"
	oFCKeditor.Create "content"
	set oFCKeditor=nothing
	%></td>
  </tr>
  <tr>
    <td class="tbLabel">首页推荐:</td>
    <td class="tbMsg"><input name="p_key" id="p_keyyes" type="radio" value="1" <%if cint(p_key)=1 then  response.write "checked"%> class="required" />
      <span class="tbLabel">是</span>
      <input name="p_key" type="radio" id="p_keyno" value="0" <%if cint(p_key)=0 then  response.write "checked"%>/>
      <span class="tbLabel">否</span>&nbsp;&nbsp;内页推荐:
      <input name="key1" id="key1yes" class="required" type="radio" value="1" <%if key1 then  response.write "checked"%> />
是
<input name="key1" type="radio" id="key1no" value="0" <%if not key1 then  response.write "checked"%>/>
否</td>
  </tr>
 <!-- <tr>
    <td class="tbLabel">是否积分产品:</td>
    <td class="tbMsg"><input name="key2" id="key2yes" class="required" type="radio" value="1" <%if key2 then  response.write "checked"%> onclick="document.getElementById('jfspan').style.display='';" />	
      是
      <input name="key2" type="radio" id="key2no" value="0" onclick="document.getElementById('jfspan').style.display='none';" <%if not key2 then  response.write "checked"%>/>
      否&nbsp;&nbsp;<span id="jfspan" <%if not key2 then response.Write("style=""display:none""")%>>所需积分:
      <input name="sxjf" id="sxjf" type="text" value="<%=sxjf%>" class="required digits" title="格式不正确!" onfocus="this.select();"/>
      </span></td>
  </tr>
  <%  if isxsp2cn then%>
    <tr>
    <td class="tbLabel">是否万客产品:</td>
    <td class="tbMsg"><input name="lyid" id="lyid" type="radio" onclick="document.getElementById('lyurlspan').style.display='';" value="1" <%if lyid=1 then  response.write "checked"%> />	
      是
     <input name="lyid" id="lyid" type="radio" value="0" onclick="document.getElementById('lyurlspan').style.display='none';" <%if lyid=0 then  response.write "checked"%>/>
      否&nbsp;&nbsp;<span id="lyurlspan" <%if lyid=0 then response.Write("style=""display:none""")%>>来源网址:
      <input name="lyurl" id="lyurl" style="width:350px" type="text" value="<%=lyurl%>" onfocus="this.select();"/>
      </span></td>
  </tr>
  <%end if%>-->
  <tr>
    <td class="tbLabel">&nbsp;</td>
    <td class="tbMsg">
    <input name="submit" type="submit" value="提交" onclick="return _sendData('form1','action.asp','result','list.asp?page=<%=request.QueryString("page")%>&sortpath2=<%=request.QueryString("sortpath2")%>',this);"/>
	<%if chkrequest(id) then response.Write("<input type=""button"" value=""将此产品作为模版添加"" onclick=""location.href='add.asp?act=add&id="&id&"';"" />")%>&nbsp;<input name="reset" type="reset" id="reset" value="重置" /></td>
  </tr> 
   <tr>
    <td class="tbLabel"></td>  
	<td class="tbMsg"><span id="result" class="warning"></span></td> 
  </tr> 
</table>
</form>
<script language="javascript">
//$(document).ready(function(){
$("#price").bind("blur",function(){		
					var price=parseFloat($("#price").val());
                    var price1=$("#price1").val();
					var price2=$("#price2").val();									
					var jf=$("#jf").val();					
					//市场价
					if(price1==''||price1=='0') $("#price1").attr("value",price*1.5);					
					//成本价
					//if(price2==''||price2=='0') $("#price2").attr("value",parseFloat(uprice1)*0.5);				
					
									
					if(jf==''||jf=='0') $("#jf").attr("value",parseInt(price));					
                }); 
//});

</script>
<%
call closeconn()
title="":keywords="":smallpic="":bigpic1="":bigpic2="":bigpic3="":bigpic4="":bigpic5="":bid="":sortpath="":content="":ms="":sxjf=""
price1="":price2="":zheke="":ks="":zd="":tuai="":fg="":serice="":hh="":kc="":p_key="":p_key1="":ljxs="":zhuangxiao="":jl="":jf="":price3="":price2=""
%>
</body>
</html>
