﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(15,qxarr)
if tqxarr(1)<>1 then showqxmsg2
dim action,sortpath,sortpathold,title,keywords,smallpic,bigpic1,bigpic2,bigpic3,bigpic4,bigpic5,bid,ljxs,content,ms,sxjf,price1,price2,zheke,ks,zd,tuai,fg,serice,hh,kc,adduser,id,p_key1,p_key,jl,zhuangxiao
dim pricearr,uflagidarr
action		= request.Form("action")
sortpath	= replace(request.Form("sortpath")," ","")
sortpathold	= replace(request.Form("sortpathold")," ","")
title		= checkstr(request.form("title"))		'名称
jl			= checkstr(request.Form("jl"))			'计量
hh			= checkstr(request.form("hh"))			'货号
smallpic	= request.form("smallpic")				'小图
bigpic1		= request.form("bigpic1")				'大图
bigpic2		= request.form("bigpic2")				'大图
bigpic3		= request.form("bigpic3")				'大图
bigpic4		= request.form("bigpic4")				'大图
bigpic5		= request.form("bigpic5")				'大图
keywords	= checkstr(request.form("keywords"))	'关键字
price		= request.Form("price")					'产品价格
price1		= request.form("price1")				'市场价格
price2		= request.form("price2")				'成本价格
jf			= request.Form("jf")					'产生积分
zheke		= request.form("zheke")					'折扣
ks			= checkstr(request.form("ks"))			'规格
zd			= checkstr(request.form("zd"))			'材质
tuai		= checkstr(request.form("tuai"))		'外箱尺寸
fg			= request.form("fg")					'商品重量
serice		= checkstr(request.form("serice"))		'体积
baozhuang	= checkstr(request.form("baozhuang"))	'包装
ljxs		= request.form("ljxs")					'累计销售
min			= request.Form("min")					'起购量
max			= request.Form("max")					'最大供应量
zhuangxiao	= request.Form("zhuangxiao")			'装箱
p_key1		= request.form("p_key1")				'推荐度
bid			= replace(request.Form("bid")," ","")	'网站栏目
color		= replace(request.form("color")," ","")	'产品颜色
size		= replace(request.form("size")," ","")	'产品尺寸
ms			= checkstr(request.form("ms"))			'商品描述
content		= request.form("content")				'详细说明
p_key		= request.form("p_key")					'是否推荐
key1		= request.form("key1")					'是否新品
key2		= request.Form("key2")					'是否积分产品
sxjf		= request.form("sxjf")					'所需积分
flag		= request.form("flag")					'是否审核
lyid		= request.form("lyid")					'来源ID
lyurl		= request.form("lyurl")					'来源网址

'-----------
dim finderr,errmsg
finderr=false
errmsg=""
if not chkRange(sortpath,1,200) then
	if not chkRange(sortpathold,1,200) then
		finderr=true
		errmsg=errmsg&"·请先选择产品分类"
	else
		sortpath=sortpathold
	end if
else
	if right(sortpath,1)="," then
		finderr=true
		errmsg=errmsg&"·请选择产品小类"
	end if
end if
if not chkrange(title,2,64) then
	finderr=true
	errmsg=errmsg&"·产品标题必需(2-64字)"
end if
'if not chkrange(jl,1,10) then
'	finderr=true
'	errmsg=errmsg&"·产品单位必需(1-10字)"
'end if
if not chkrange(hh,2,50) then
	finderr=true
	errmsg=errmsg&"·货号必需(2-50字)"
end if
if not chkRange(smallpic,10,100) then
	finderr=true
	errmsg=errmsg&"·小图必需"
end if
if not chkRange(bigpic1,10,100) then
	finderr=true
	errmsg=errmsg&"·大图必需"
end if
if not chkRange(keywords,1,100) then
	finderr=true
	errmsg=errmsg&"·关键字必需"
end if
if not chkNumber(price) then
	finderr=true
	errmsg=errmsg&"·产品价格必需"
end if
if not chkNumber(price1) then
	finderr=true
	errmsg=errmsg&"·原价必需"
end if
'if not chkNumber(price2) then
'	finderr=true
'	errmsg=errmsg&"·成本价格必需"
'end if
'if not chkNumber(fg) then
'	finderr=true
'	errmsg=errmsg&"·产品重量必须 请填写数字 单位为千克"
'end if
'if chkrequest(lyid) and not chkRange(lyurl,5,150) then
'	finderr=true
'	errmsg=errmsg&"·请输入产品来源的网站(要具体的产品详细页网站)"
'end if
if not chkrequest(jf) then jf=clng(price)
if not chkrequest(zhuangxiao) then zhuangxiao=0
if not chkrequest(sxjf) then sxjf=0
if not chkNumber(zheke) then zheke=1
if ks<>"" then ks=left(ks,50)
if zd<>"" then zd=left(zd,50)
if tuai<>"" then tuai=left(tuai,50)
if serice<>"" then serice=left(serice,50)
if baozhuang<>"" then baozhuang=left(baozhuang,50)
if not chkrequest(ljxs) then ljxs=0
if not chkrequest(min) then min=1
if not chknumber(max) then max=0
if not chkrequest(p_key1) then p_key1=3
if not chkPara(bid) then bid="0"
if not chkPara(color)  then 
	color="0"
end if
if not chkPara(size)  then 
	size="0"
end if
if not chkrange(ms,10,150) then
	finderr=true
	errmsg=errmsg&"·描述为必需(10-150字)"
end if
if chkNull(content,10) then
	finderr=true
	errmsg=errmsg&"·内容为必需(10字以上)"
end if

if p_key="1" then p_key=1 else p_key=0
if key1="1" then key1=1 else key1=0
if key2="1" then key2=1 else key2=0
'if key2=1 then 
'	if not chkrequest(sxjf) then
'		finderr=true
'		errmsg=errmsg&"·积分产品所需积分必需大于0"
'	end if
'end if
flag=1
if chkrequest(lyid) then lyid=cint(lyid) else lyid=0
if finderr then
	response.Write(errmsg)
	call closeconn()
	response.End()
end if
finderr="":errmsg=""

select case action
	case "add"
		sql="select top 1 id from products where hh='"&hh&"'"
		tarr=getdbvalue(sql,1)
		if tarr(0)<>0 then
			response.Write("·产品编号已经存在!请使用其它的产品编号")
			call closeconn()
			response.End()
		end if
		erase tarr
 		set rs=server.CreateObject("adodb.recordset")
		rs.open "select * from products",conn,1,3
		rs.addnew
		rs("adduser")	= user	
		rs("hits")		= 1	
		rs("flag")		= flag		
		rs("createdon")	= gettime()
		rs("hh")		= hh		'货号
		call updateData()
		id=conn.execute("select @@identity as 'id'")(0)
		actionname="添加"
	case "edit"
		id=request.form("id")
		if not chkrequest(id) then 
			response.Write("·ID参数丢失")
			call closeconn()
			response.End()
		end if	
		
		set rs=server.CreateObject("adodb.recordset")
		rs.open "select top 1 * from products where id="&clng(id),conn,1,3
		if 	not rs.eof then	
			oldhh=rs("hh")		
			if oldhh<>hh then
				response.Write("·产品编号不能修改")
				call closers(rs)
				call closeconn()	
				response.End()		
			end if
			call updateData()					
		end if
		actionname="修改"
end select 
if webhtml=3 then
a=createHtmlProductDetail(id)
else
a=0
end if
if a<>0 then
	response.Write("·生成产品静态失败，错误ID："&a)
else
	response.Write("·产品成功"&actionname)
end if
call closers(rs)
call closeconn()
response.End()

sub updateData()
rs("sortpath")	= sortpath	'商品类别路径
rs("name")		= title		'名称
rs("jl")		= jl		'计量
rs("smallpic")	= smallpic	'小图
rs("bigpic1")	= bigpic1	'大图
rs("bigpic2")	= bigpic2	'大图
rs("bigpic3")	= bigpic3	'大图
rs("bigpic4")	= bigpic4	'大图
rs("bigpic5")	= bigpic5	'大图
rs("keywords")	= keywords	'关键字
rs("price")		= cdbl(price)		'产品价格
rs("price1")	= cdbl(price1)	'市场价格
rs("price2")	= cdbl(price2)	'成本价格
rs("jf")		= jf		'产生的积分
rs("zheke")		= zheke		'折扣
rs("ks")		= ks		'款式
rs("zd")		= zd		'主要质地
rs("tuai")		= tuai		'图案
rs("fg")		= cdbl(fg)	'风格
rs("serice")	= serice	'适合季节
rs("baozhuang")	= baozhuang	'包装
rs("ljxs")		= ljxs		'累计销售
rs("min")		= min		'最小起订
rs("max")		= max		'最多供应
rs("zhuangxiao")= zhuangxiao'装箱
rs("p_key1")	= p_key1	' 推荐度
rs("bid")		= bid		'网站栏目
rs("color")		= color		'颜色
rs("size")		= size		'尺寸
rs("ms")		= ms		'商品描述
rs("content")	= replace_text(content)	'详细说明
rs("p_key")		= p_key		'是否推荐
if p_key=1 or key1=1 then
	rs("keytime")	= now()
end if
rs("key1")		= key1		'是否新品
rs("key2")		= key2		'是否积分产品
rs("sxjf")		= sxjf		'购买此产品所需要的积分
rs("updatetime")=gettime()		'更新时间
rs("lyid")=lyid
if len(lyurl)>5 then
rs("lyurl")=lyurl
end if
rs.update
end sub

%>