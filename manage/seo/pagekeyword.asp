﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(6,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="pagekeyword.asp">页面关键字管理</a>
	</td>
</tr>
</table>
<%
sql="select id,[name],url from pagekeyword order by id desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center" width="25%">页面名称</td>
	<td align="center" width="68%">页面地址</td>	
	<td align="center" width="7%">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 	
	for i=0 to ubound(tarr,2)		
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tarr(1,i)%></td>
	<td align="center"><%=tarr(2,i)%></td>	
	<td align="center"><!--<a href="pagekeyword_save.asp?id=<%=tarr(0,i)%>&myAction=deleteSort" onclick="return confirm('该操作不能恢复，确认吗？');">删除</a> | --><a href="pagekeyword.asp?pc=editSort&id=<%=tarr(0,i)%>">修改</a> </td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="5" align="right"><a href="pagekeyword.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	id=clng(request.QueryString("id"))
	name="":title="":keyword="":desc="":memo="":url=""
	if pc="editSort" then
		if tqxarr(1)<>1 then showqxmsg
		sql="select [name],title,keyword,[desc],memo,url from pagekeyword where id="&id
		tarr=getdbvalue(sql,6)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		name=tarr(1):title=tarr(2):keyword=tarr(3):desc=tarr(4):memo=tarr(5):url=tarr(6)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="pagekeyword_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">页面名称：</td>
      <td width="86%" align="left" bgcolor="#FFFFFF"><input name="name" type="text" value="<%=name%>" size="40" dataType="LimitB" min="1" max="50" msg="页面名称必须为1-50个字以内" readonly>
        <span class="red">*</span>
      相对地址：
      <input name="url" type="text" id="url" value="<%=url%>" size="45" datatype="LimitB" min="1" max="100" msg="相对地必须为100个字以内" require="false"  readonly/>
      <span class="red">*</span></td>
   </tr>
     <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">页面标题：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="title" type="text" value="<%=title%>" size="80" dataType="LimitB" min="2" max="300" msg="页面标题必须为2-300个字以内">
        <span class="red">*</span> </td>
   </tr>
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">关 键 字：</td>
      <td align="left" bgcolor="#FFFFFF"><textarea name="keyword" cols="80" rows="3" datatype="LimitB" min="2" max="500" msg="关键字必须为2-500个字以内" ><%=keyword%></textarea>
        <span class="red">*</span> </td>
   </tr> 
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">页面描叙：</td>
      <td align="left" bgcolor="#FFFFFF"><textarea name="desc" cols="80" rows="3" datatype="LimitB" min="2" max="500" msg="页面描叙必须为2-500个字符" ><%=desc%></textarea>
        <span class="red">*</span> </td>
   </tr> <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">变量说明：</td>
      <td align="left" bgcolor="#FFFFFF"><textarea name="memo" cols="80" rows="3" datatype="LimitB" min="2" max="300" msg="变量说明请在300在以内" Require="false" readonly="readonly"><%=memo%></textarea>
        <span class="red">*</span> </td>
   </tr>
     
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		   （<span class="red">注：标题，关键字，描叙均支持系统标签</span>）		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>