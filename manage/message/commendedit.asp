﻿<!--#include file="../top.asp"-->

<%
tqxarr=getqx(19,qxarr)
if tqxarr(1)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>评论回复</title>
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>

<body>
<%
id=request.QueryString("id")
act  = request.QueryString("act")
if act = "edit" then 
	flag	= request.Form("flag")
	if flag<>"1" then flag=0
	update_record "message","recontent='"&request.Form("recontent")&"',recreatedon='"&gettime()&"',reuser='"&request.Form("reuser")&"',flag=" & flag ,"id="&clng(id)&""
	
	call closeconn()
	response.Redirect("commend.asp")
	response.End()
end if
if not isnumeric(id) or id="" then 
	showmsg("参数传递出错!")
end if 
set rs=conn.execute("select top 1 * from message where id="&clng(id))
if not rs.eof then 
	title		= rs("title")
	useremail	= rs("useremail")
	truename	= rs("truename")
	tel			= rs("tel")
	address		= rs("address")
	content		= rs("content")
	recontent	= rs("recontent")
	pid			= rs("pid")
	flag		= rs("flag")
	'reuser		= rs("reuser")
end if 
rs.close
set rs=nothing
call closeconn()
%>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > <a href="commend.asp">评论回复</a> > </td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form action="?act=edit&id=<%=id%>"  name="form1" method="post" onsubmit="return Validator.Validate(this,3);">
  <tr>
    <td class="tbLabel">标题:</td>
    <td class="tbMsg"><%=title%></td>
  </tr>
  <tr>
    <td class="tbLabel">留言人Email:</td>
    <td class="tbMsg"><%=useremail%></td>
  </tr>
  <tr>
    <td class="tbLabel">姓名:</td>
    <td class="tbMsg"><%=truename%></td>
  </tr>
  <!-- <tr>
    <td class="tbLabel">电话:</td>
    <td class="tbMsg"><%=tel%></td>
  </tr>
   <tr>
    <td class="tbLabel">地址:</td>
    <td class="tbMsg"><%=address%></td>
  </tr>-->
  <tr>
    <td class="tbLabel">内容:</td>
    <td class="tbMsg">
	<%=content%>
	</td>
  </tr>
  <tr>
    <td class="tbLabel">显示状态:</td>
    <td class="tbMsg">
	<input type="radio" name="flag" value="0" <%if flag=0 then response.Write("checked")%>/>隐藏&nbsp;<input type="radio" name="flag" value="1" <%if flag=1 then response.Write("checked")%>/>显示
	</td>
  </tr>
  <tr>
    <td class="tbLabel">回复内容:</td>
    <td class="tbMsg"><textarea name="recontent" cols="50" rows="5" dataType="Require" msg="回复内容!"><%=recontent%></textarea></td>
  </tr>
  <input type="hidden" name="pid" value="<%=pid%>" />
  <tr>
    <td class="tbLabel">回复人:</td>
    <td class="tbMsg"><input name="reuser" type="text" value="<%=request.Cookies("adminLogin")("admin")%>" size="10" dataType="Require" msg="回复人!" readonly="readonly"/></td>
  </tr>
  <tr>
    <td class="tbLabel"></td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" />
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
 </form>
</table>
</body>
</html>
