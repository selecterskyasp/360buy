﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(5,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css" href="../style/css.css">
<link rel="stylesheet" type="text/css" href="../style/common.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<script language="javascript" src="<%=webvirtual%>/js/jquery.js"></script>
</head>
<style>
.list {border:1px solid #ff3300; margin:2px; padding-left:5px}
</style>

<body>
<%
m=request("m")
p=request("p")
action=request.QueryString("action")
if chknull(m,1) then alert "模版路径不对","",1
if action="save" and chknull(p,1) then alert "文件路径丢失","",1
if chknull(p,1) then p="index.html"
f="/skin/"&m&"/"&p
if not isexists(f) then alert "文件不存在"&f,"",1
dim action
pname="home"
if instr(p,"/")>0 then
	parr=split(p,"/")
	pname=parr(0)
end if

if action="save" then
	if tqxarr(1)<>1 then showqxmsg
	content=request.Form("content")
	if chknull(content,10) then alert "模版内容不能为空","",1
	content=re_t(content)	
	tarr=savetofileauto(content,f,"utf-8")
	if tarr(0)<>0 then alert "保存模版内容到文件失败，原因："&tarr(1)&" 当前的操作没有执行！","",1	
	content="":erase tarr
	if instr(p,".js")>0 then
		
		select case p
		case "js/pagehead.js"
			call createPageheadJS()
		case "js/pagetop.js"
			call createPagetopJS()
		case "js/pagemenu.js"
			call createPagemenuJS()
		case "js/pageSearch.js"
			call createPagesearchJS()
		end select			
	end if	
end if	

function re_text(st)	
	st=replace(st,"<","&lt;")
	st=replace(st,">","&gt;")
	st=replace(st,"""","&quot;")
	re_text=st	
end function

function re_t(st)
	st=replace(st,"&lt;","<")
	st=replace(st,"&gt;",">")
	st=replace(st,"&quot;","""")
	re_t=st
end function
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1">
<form name="form1" method="post" action="edit.asp?action=save" onSubmit="return Validator.Validate(this,2);">
<tr>
    <td>	
	<input type="hidden" name="p" value="<%=p%>" />
	<input type="hidden" name="m" value="<%=m%>" />
	请选择模版分类：<select name="fl" onchange="change(this)">	
	<option value="home" <%if pname="home" then response.Write("selected")%>>首页</option>
	<option value="products" <%if pname="products" then response.Write("selected")%>>产品</option>
	<option value="news" <%if pname="news" then response.Write("selected")%>>资讯</option>
	<option value="server" <%if pname="server" then response.Write("selected")%>>帮助</option>
	<option value="about" <%if pname="about" then response.Write("selected")%>>关于我们</option>
	<option value="reg" <%if pname="reg" then response.Write("selected")%>>注册页面</option>	
	<option value="pay" <%if pname="pay" then response.Write("selected")%>>支付模版</option>
	<option value="categories" <%if pname="categories" then response.Write("selected")%>>分类列表</option>
	<option value="js" <%if pname="js" then response.Write("selected")%>>静态页JS</option>
	</select>  <span class="red">警告：修改模版前请切记做好备份。以防意外发生！</span>
	<div id="home" class="list"><a href="?m=<%=m%>&p=index.html">首页</a> | <a href="?m=<%=m%>&p=head.html">头部</a> | <a href="?m=<%=m%>&p=detailhead.html">详细页头部</a> | <a href="?m=<%=m%>&p=foot.html">脚部</a></div>
	<div id="products" class="list"><a href="?m=<%=m%>&p=products/index.html">产品首页</a> | <a href="?m=<%=m%>&p=products/detail.htm">产品详细页</a> | <a href="?m=<%=m%>&p=products/my_cart.html">购物车</a> | <a href="?m=<%=m%>&p=products/allprice.html">批量下单</a> | <a href="?m=<%=m%>&p=products/confirm_order.html">订单页</a></div>
	<div id="news" class="list"><a href="?m=<%=m%>&p=news/index.html">新闻首页</a> | <a href="?m=<%=m%>&p=news/detail.htm">新闻详细页</a></div>
	<div id="server" class="list"><a href="?m=<%=m%>&p=server/index.html">帮助首页</a></div>
	<div id="about" class="list"><a href="?m=<%=m%>&p=about/index.html">关于我们首页</a> | <a href="?m=<%=m%>&p=about/feedback.html">留言反馈</a> | <a href="?m=<%=m%>&p=about/links.html">友情链接</a></div>
	<div id="reg" class="list"><a href="?m=<%=m%>&p=reg/index.html">登陆首页</a> | <a href="?m=<%=m%>&p=reg/reguser.html">注册页面</a> | <a href="?m=<%=m%>&p=reg/reguser2.html">注册完成</a> | <a href="?m=<%=m%>&p=reg/jihuo.html">激活页面</a> | <a href="?m=<%=m%>&p=reg/resetPass.html">重置密码</a> | <a href="?m=<%=m%>&p=reg/agreement.html">注册条约</a></div>
	<div id="pay" class="list"><a href="?m=<%=m%>&p=pay/bankpay/index.html">银行支付</a> | <a href="?m=<%=m%>&p=pay/yucunpay/index.html">预存款支付</a></div>	
	<div id="js" class="list"><a href="?m=<%=m%>&p=js/pagehead.js">样式调用</a> | <a href="?m=<%=m%>&p=js/pagetop.js">头部信息</a> | <a href="?m=<%=m%>&p=js/pagemenu.js">网站栏目</a> | <a href="?m=<%=m%>&p=js/pageSearch.js">搜索栏目</a></div>	
	<div id="categories" class="list"><a href="?m=<%=m%>&p=categories/index.html">所有产品分类</a></div>
	</td>
</tr>
<tr>
<td>
<textarea name="content" cols="120" rows="30" dataType="Require" msg="模版内容不能为空"><%=re_text(getcache(p))%></textarea>
</td>
</tr>
<tr bgcolor="#ffffff" align="center">
    <td height="30" colspan=4><input type="submit" name="Submit" value="提 交" class=input1>&nbsp;<input type="button" value="返回模版列表页面" onclick="location.href='list.asp'" /></td>
</tr>
</form>
<%
call closeconn()%>
</table>
<script language="javascript">
function change(obj)
{
	if(obj.value=='') return;	
	$(".list").css("display","none");
	$("#"+obj.value).css("display","");
	//document.getElementById(obj.value).style.display="black";	
	//alert("sf");
}
$(".list").css("display","none");
$("#"+"<%=pname%>").css("display","");
</script>
</body>
</html>
