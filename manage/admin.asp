﻿<!--#include file="top.asp"-->
<!--#include file="../inc/md5.asp"-->
<%
tqxarr=getqx(14,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员信息</title>
<link href="style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>

<body>
<%
action1=request.form("action1")
action=request.QueryString("action")
'response.write action1
if action1 = "edit" or action1="add" then
	if tqxarr(1)<>1 then showqxmsg
	a=checkstr(request.Form("admin"))
	p=checkstr(request.Form("password"))
	t=request.Form("zu")
	id=request.Form("id")
	if not chkrequest(t) then alert "请选择管理员分组","",1	
	if not chkrequest(id) and action1="edit" then alert "error","",1
	
	'检查分组权限
	sql="select top 1 ms from curclass where id="&t
	tarr=getdbvalue(sql,1)
	if tarr(0)=0 then alert "分组不存在，请确认","",1
	ms=tarr(1)
	erase tarr	
	if not chkqxvaild(ms,1) then alert "分组权限无效，请进入分组管理重新分配此分组的权限","",1
	
	'输入有效性检查
	if not chkrange(a,3,20) and action1="add" then alert "管理员用户名长度必须为3-20字符","",1
	if not chkrange(p,3,20) and (action1="add" or len(p)>1) then alert "管理员密码必须为3-20字符","",1
	if action1="add" then
		sql="insert into admin (admin,password,mmid,qx,adddate,lastlogin,ip,num,[type])values('"&a&"','"&md5(p)&"',0,'"&ms&"','"&now()&"','','',1,"&t&")"
	else
		if chkRange(p,3,20) then st=",password='"&md5(p)&"'" else st=""
		sql="select [type],[admin] from [admin] where id="&id
		tarr=getdbvalue(sql,2)
		if tarr(0)=0 then alert "管理员不存在","",1
		old=clng(tarr(1))
		if trim(tarr(2))="admin" then ms=qxchushiall
		'if old<>clng(t) and (clng(id)=adminid or trim(tarr(2))="admin") then alert "您不能设置自己的分组/超级管理员不能修改权限组","",1		
		erase tarr
		sql="update admin set [type]="&t&",qx='"&ms&"'"&st&" where id="&id		
	end if	
	call setdbvalue(sql)
	alert "添加/修改管理成功，如果该管理员已经登陆，请用新密码重新登陆","0",0
end if 

if action="del" then
	if tqxarr(2)<>1 then showqxmsg
	  id=clng(request.QueryString("id"))
	  sql="select [admin] from [admin] where id="&id
	  tarr=getdbvalue(sql,1)
	  if tarr(0)=0 then alert "管理员不存在","",1
	  if trim(tarr(1))="admin" then alert "超级管理员不能删除","",1	  
	  sql="delete admin where id="&id
	  call setdbvalue(sql)
end if
%>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > 系统信息</td>
  </tr>
</table>
<table class="MsgTbl" border="0" cellspacing="0" cellpadding="0">
 <tr>
    <td class="tbHeader">登录名</td>
    <td class="tbHeader">分组</td>
    <td class="tbHeader">操作</td>
  </tr>
  <%  	
  	sql ="select id,admin,[type] from admin"
 	set rs=server.CreateObject("adodb.recordset")
	rs.open sql,conn,1,1
	if not rs.eof then 
		for i=1 to rs.recordcount
		tname=getcurnamelist(rs("type"))
  %>
	  <tr>
		<td class="tbMsg"><%=rs("admin")%></td>
		<td class="tbMsg"><%=tname%></td>
		<td class="tbMsg"><a href="?action=del&id=<%=rs("id")%>" onclick="return confirm('删除管理员不能恢复，确实要删除吗？');">删除</a> | <a href="?action=ed&id=<%=rs("id")%>">修改</a> | <a href="adminqx.asp?t=0&id=<%=rs("id")%>">单独设置权限</a></td>
	  </tr>
  <%
	  rs.movenext
	  next
  end if 
  rs.close
  set rs=nothing
  %>
  <tr>
 	<td class="tbFooter" colspan="3"><a href="admin.asp?action=add">添加</a></td>
 </tr>
</table>
<%

if action="add" or action="ed" then
if tqxarr(1)<>1 then showqxmsg
admin="":ty=0:password=""
%>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form action="admin.asp"  name="form1" method="post" onsubmit="return Validator.Validate(this,3);">
	<%
	
	if action="ed" then 
		id=request.QueryString("id")
		if not chkrequest(id) then  alert "error","",1
		sql="select top 1 admin,[type] from admin where id="&id
		tarr=getdbvalue(sql,2)
		if tarr(0)=0 then alert "此管理员不存在","",1
		admin=tarr(1)
		if chkrequest(tarr(2)) then ty=clng(tarr(2)) else ty=0
	%>
		<input type="hidden" name="id" value="<%=id%>">
		<input type="hidden" name="action1" value="edit">
	<%else%>
		<input type="hidden" name="action1" value="add">
	<%end if%>
  <tr>
    <td class="tbLabel">帐号:</td>
    <td class="tbMsg"><input name="admin" type="text" value="<%=admin%>" dataType="Require" msg="请输入管理员帐号!"<%if action="ed" then response.write("readonly") end if %>  /></td>
  </tr>
  <tr>
    <td class="tbLabel">密码:</td>
    <td class="tbMsg"><input name="password" type="password"/><%if action="ed" then response.Write("<span class=yellow>如果您不修改密码请留空</span>")%></td>
  </tr>
    <tr>
    <td class="tbLabel">所在组:</td>
    <td class="tbMsg"><select name="zu" id="zu"><%=getOptionNameList(27,ty,1)%></select></td>
  </tr>
  <tr>
    <td class="tbLabel"></td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" />
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
 </form>
</table>
<%end if %>
</body>
</html>
