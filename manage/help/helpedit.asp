﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(20,qxarr)
if tqxarr(1)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>帮助添加与修改</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>

<body>
<!--#include file="../../fckeditor/fckeditor.asp"-->
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > <a href="list.asp">帮助管理</a> > 帮助添加与修改</td>
  </tr>
</table>
<%
action=request("action")
if action="add" or action="edit" then
	title=checkstr(request.Form("title"))
	classid=request.Form("classid")
	keyword=checkstr(request.Form("keyword"))
	ms=checkstr(request.Form("ms"))
	sourcer=checkstr(request.Form("sourcer"))
	content=request.Form("content")
	id=request("id")
	ferr=false
	msg=""
	if not chkRange(title,2,50) then 
	ferr=true
	msg=msg&"\n·请输入信息标题2-50字以内"
	end if
	if not chkrequest(classid) then  classid=0
	'ferr=true
'	msg=msg&"\n·请选择信息分类"
'	end if
	if not chkRange(keyword,2,200) then 
	ferr=true
	msg=msg&"\n·请输入信息关键字2-200字以内"
	end if
	if ms<>"" and len(ms)>500 then 
		ferr=true
		msg=msg&"\n·信息描叙请在500字以内"
	end if
	if not chkRange(sourcer,1,20) then 
	ferr=true
	msg=msg&"\n·请输入信息来源1-20字以内"
	end if
	if chknull(content,10) then 
	ferr=true
	msg=msg&"\n·请输入信息内容10字以上"
	end if
	if action="edit" and not chkrequest(id) then
		ferr=true
		msg=msg&"\n·ID号丢失"
	end if
	if ferr then alert msg,"",1
end if
select case action
	case "add"	
		sql="insert into help (name,bid,keywords,ly,content,hits,createdon,adduser,flag,ms)values('"&title&"',"&classid&",'"&keyword&"','"&sourcer&"','"&replace_text(content)&"',0,'"&gettime()&"','"&request.Cookies("adminLogin")("admin")&"',1,'"&ms&"')"
		conn.execute(sql)
		response.Redirect("list.asp")
		response.End()
	case "edit"		
		sql="update help set name='"&title&"',bid="&classid&",keywords='"&keyword&"',ly='"&Sourcer&"',content='"&replace_text(content)&"',ms='"&ms&"' where id="&clng(id)
		conn.execute(sql)
		response.Redirect("helpedit.asp?id="&id&"")
		response.End()
end select
id=request.querystring("id")
if id="" then 
	title		= ""	
	classid		= 0	
	keyword		= ""
	ly			= ""
	content		= ""
	Sourcer		= webname
	action		= "add"
else
set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 * from help where id="&cint(id),conn,1,1
if not rs.eof then 
	title		= rs("name")	
	classid		= rs("bid")	
	keyword		= rs("keywords")
	ms			= rs("ms")
	Sourcer		= rs("ly")
	content		= replace_t(rs("content"))
	action		= "edit"
end if 
rs.close
set rs=nothing
end if 

%>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form action="helpedit.asp"  name="form1" method="post" onsubmit="return Validator.Validate(this,3);">
	<input type="hidden" name="action" value="<%=action%>">
	<%if action="edit" then %>
	<input type="hidden" name="id" value="<%=id%>">
	<%end if%>
  <tr>
    <td class="tbLabel">信息标题:</td>
    <td class="tbMsg"><input name="title" type="text" value="<%=title%>" dataType="Require" msg="请输入帮助标题!" size="80"/></td>
  </tr>
  <!--<tr>
    <td class="tbLabel">帮助类别:</td>
    <td class="tbMsg">
	<select name="classid" dataType="Require" msg="请选择帮助类别!">
	<%
	set rs=server.createobject("adodb.recordset")
	rs.open "select id,name from curclass where parentid=1",conn,1,1
	if not rs.eof then 
	 	do while not rs.eof 
	%>
		<option value="<%=rs("id")%>" <%if cint(rs("id")) = cint(classid) then response.write "selected"%>><%=rs("name")%></option>
	<%
		rs.movenext
		loop
	end if 
	rs.close
	set rs=nothing
	%>
	</select>	</td>
  </tr>-->
  <tr>
    <td class="tbLabel">关 键 字:</td>
    <td class="tbMsg"><input name="keyword" type="text" value="<%=keyword%>" size="65" dataType="LimitB" min="1" max="200" msg="请输入关键字!"/>
    （多个关键字以,分隔）</td>
  </tr>
    <tr>
    <td class="tbLabel">页面描叙:</td>
    <td class="tbMsg"><textarea name="ms" cols="80" datatype="LimitB" min="1" max="500" msg="描叙为500字以内" require="false"><%=ms%></textarea>
      <br />
      （留空为提取内容的前150字）</td>
  </tr>
  <tr>
    <td class="tbLabel">详细内容:</td>
    <td class="tbMsg">
	<%
	'Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath =webvirtual&"/fckeditor/"
	oFCKeditor.Value	= content
	oFCKeditor.height	= "350"
	oFCKeditor.Create "content"
	%>	</td>
  </tr>
  <tr>
    <td class="tbLabel">帮助来源:</td>
    <td class="tbMsg"><input name="Sourcer" type="text" value="<%=Sourcer%>" size="10" dataType="Require" msg="帮助来源!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">&nbsp;</td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" />
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
 </form>
</table>
<%
call closeconn()
%>
</body>
</html>
