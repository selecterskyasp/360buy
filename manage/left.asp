<!--#include file="top.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2009 www.intltj.com" />
<meta name="Keywords" content="Yiwu Tujin Co., Ltd." />
<meta name="Description" content="Yiwu Tujin Co., Ltd." />
<link rel="stylesheet" href="style/common.css" type="text/css" media="all" />
<title>Content Management System</title>
<style type="text/css">
<!--
body{background:#f7f7f7;overflow-x:hidden;overflow-y:auto }
-->
</style>
</head>
<body>	

<!--content begin-->

<div class="SiderBar">
<div class="SiderBarTitle">管理系统</div>
</div>
<ul class="MenuBar">
 <li class="item"><a href="javascript:;" class="title" name="1">系统设置</a>
   <ul id="opt_1" class="optiton">    
    <li>·<a href="config.asp" target="iframeright">系统配置</a></li>
	<li>·<a href="system/lanmu.asp" target="iframeright">网站栏目设置</a></li>		
	<li>·<a href="system/class.asp" target="iframeright">分类设置</a></li>		
	<!--<li>·<a href="seo/googlemaps.asp" target="iframeright">网站地图生成</a></li>-->
	<li>·<a href="link/links.asp" target="iframeright">友情连接</a> <a href="link/linkskey.asp" target="iframeright" name="right">待审</a></li>
   </ul>
 </li>
 <li class="item"><a href="javascript:;" class="title" name="2">管理员管理</a>
   <ul id="opt_2" class="optiton" style="display:none">
	<li>·<a href="admin.asp" target="iframeright">管理员管理</a></li>
    <li>·<a href="admingroup.asp" target="iframeright">管理员分组管理</a></li>
   </ul>
 </li>
 <li class="item"><a href="javascript:;" class="title" name="3">商品管理</a>
   <ul id="opt_3" class="optiton" style="display:none">
   
    <li>·<a href="product/add.asp?act=add" target="iframeright">添加商品</a></li>
	<li>·<a href="system/moveclass.asp" target="iframeright">移动产品</a></li>
    <li>·<a href="product/list.asp" target="iframeright">所有商品</a></li>	
	
   </ul>
 </li>
 
  <!--<li class="item"><a href="javascript:;" class="title" name="5">浏览/购买记录</a>
   <ul id="opt_5" class="optiton" style="display:none">
    <li>·<a href="records/list.asp" target="iframeright">浏览记录</a></li>
    <li>·<a href="records/list1.asp" target="iframeright">购买记录</a></li>
    <li>·<a href="records/p_commend.asp" target="iframeright">评论记录</a></li>
    <li>·<a href="records/p_buy.asp" target="iframeright">产品销售排行</a></li>
	 <li>·<a href="records/p_hits.asp" target="iframeright">产品点击率</a></li>
    <li>·<a href="records/p_ting.asp" target="iframeright">产品停留时间</a></li>
	 <li>·<a href="records/p_dang.asp" target="iframeright">会员下单时间曲线</a></li>
	 <li>·<a href="records/p_first.asp" target="iframeright">第一次购买时长</a></li>
    <li>·<a href="records/p_first.asp" target="iframeright">会员重复购买统计</a></li>
   </ul>
 </li>-->
<!--  <li class="item"><a href="javascript:;" class="title" name="6">信息管理</a>
   <ul id="opt_6" class="optiton" style="display:none">
    <li>·<a href="system/curclass.asp?parentid=2" target="iframeright">信息分类</a></li>
    <li>·<a href="news/newsedit.asp" target="iframeright">添加信息</a></li>
    <li>·<a href="news/list.asp" target="iframeright">信息管理</a></li>    
   </ul>
 </li>-->
 <li class="item"><a href="javascript:;" class="title" name="7">意见及评论</a>
   <ul id="opt_7" class="optiton" style="display:none">
    <li>·<a href="message/book.asp" target="iframeright">反馈查看</a></li>
    <li>·<a href="message/commend.asp" target="iframeright">商品评论</a></li>
    <li>·<a href="message/ping.asp" target="iframeright">团购报名</a></li>    
   </ul>
 </li> 
<li class="item"><a href="javascript:;" class="title" name="8">帮助文档</a>
   <ul id="opt_8" class="optiton" style="display:none">
   <!-- <li>·<a href="system/curclass.asp?parentid=1" target="iframeright">帮助分类</a></li>-->
    <li>·<a href="help/list.asp" target="iframeright">帮助管理</a></li>   
	 <li>·<a href="help/helpedit.asp" target="iframeright">帮助添加</a></li> 
	 
   </ul>
 </li>
  <!--<li class="item"><a href="javascript:;" class="title" name="9">库存管理</a>
   <ul id="opt_9" class="optiton" style="display:none">     	
	 <li>·<a href="product/addkucen.asp" target="iframeright">添加库存</a></li>
    <li>·<a href="product/list.asp?kucen=0" target="iframeright">缺货商品</a></li>
    <li>·<a href="product/list.asp?kucen=10" target="iframeright">须进货商品</a></li>
	<li>·<a href="product/list.asp?kucen=-1" target="iframeright">无限供应商品</a></li>	 
	 <li>·<a href="sell/ruku.asp" target="iframeright">入库详细</a></li>
    <li>·<a href="sell/chuku.asp" target="iframeright">出库详细</a></li> 
   </ul>
 </li>
   <li class="item"><a href="javascript:;" class="title" name="10">广告管理</a>
   <ul id="opt_10" class="optiton" style="display:none">     	
	 <li>·<a href="ad/scroll.asp" target="iframeright">首页图片展示</a></li>
	 <li>·<a href="ad/mana.asp" target="iframeright">类别管理</a></li>  	
 	 <li>·<a href="ad/list.asp" target="iframeright" name="right">广告管理</a></li>
   </ul>
 </li>
 <li class="item"><a href="javascript:;" class="title" name="11">客服管理</a>
   <ul id="opt_11" class="optiton" style="display:none">     	
	<li>·<a href="kf/class.asp" target="iframeright">客服管理</a></li>	
	<li>·<a href="system/curclass.asp?parentid=17" target="iframeright">客服服务分类</a></li>	
	<li>·<a href="system/curclass.asp?parentid=14" target="iframeright">客服号码分类</a></li>	 
   </ul>
 </li>-->
</ul>

<!--content end-->

 <script language="javascript" type="text/javascript">
 // --- 获取ClassName
 document.getElementsByClassName = function(cl) {
  var retnode = [];
  var myclass = new RegExp('\\b'+cl+'\\b');
  var elem = this.getElementsByTagName('*');
  for (var j = 0; j < elem.length; j++) {
   var classes = elem[j].className;
   if (myclass.test(classes)) retnode.push(elem[j]);
  }
  return retnode;
 }
 
 // --- 隐藏所有
 function HideAll() {
  var items = document.getElementsByClassName("optiton");
  for (var j=0; j<items.length; j++) {
   items[j].style.display = "none";
  }
 }
 
 // --- 设置cookie
 function setCookie(sName,sValue,expireHours) {
  var cookieString = sName + "=" + escape(sValue);
  //;判断是否设置过期时间
  if (expireHours>0) {
    var date = new Date();
    date.setTime(date.getTime + expireHours * 3600 * 1000);
    cookieString = cookieString + "; expire=" + date.toGMTString();
  }
  document.cookie = cookieString;
 }
 
 //--- 获取cookie
 function getCookie(sName) {
   var aCookie = document.cookie.split("; ");
   for (var j=0; j < aCookie.length; j++){
  var aCrumb = aCookie[j].split("=");
  if (escape(sName) == aCrumb[0])
    return unescape(aCrumb[1]);
   }
   return null;
 }
 
 window.onload = function() {
  var show_item = "opt_1";
  if (getCookie("show_item") != null) {
    show_item= "opt_" + getCookie("show_item");
  }
  document.getElementById(show_item).style.display = "block";
  var items = document.getElementsByClassName("title");
  for (var j=0; j<items.length; j++) {
   items[j].onclick = function() {
    var o = document.getElementById("opt_" + this.name);
    if (o.style.display != "block") {
     HideAll();
     o.style.display = "block";
     setCookie("show_item",this.name);
    }
    else {
     o.style.display = "none";
    }
   }
  }
 }
 </script>
<%call closeconn()%>
</body>
</html>