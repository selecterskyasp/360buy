﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(25,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="class.asp">会员等级管理</a>
	</td>
</tr>
</table>
<%
sql="select id,[num],type,type2,flag from kf order by adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">号码</td>
	<td align="center">类型</td>
	<td align="center">状态</td>
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 
	dim tempid,nextcount,ttarr,tparentid,prelink
	for i=0 to ubound(tarr,2)
		tempid=tarr(0,i)
		name=getcurnamelist(tarr(2,i))
		name2=getcurnamelist(tarr(3,i))	
		
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tarr(1,i)%></td>
	<td align="center"><%=name%>/<%=name2%></td>
	<td align="center"><%if tarr(4,i) then response.Write("<font color=green>已显示</font>") else response.Write("<font color=red>已隐藏</font>")%></td>
	<td align="center"><a href="class_save.asp?id=<%=tempid%>&myAction=deleteSort" onclick="return confirm('该操作将会删除属于该等级的会员和产品价格信息不能恢复，确认吗？');">删除</a> | <a href="class.asp?pc=editSort&id=<%=tempid%>">修改</a> | <a href="class_save.asp?id=<%=tempid%>&myAction=sort">排序</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="4" align="right"><a href="class.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	if tqxarr(1)<>1 then showqxmsg
	id=request.QueryString("id")
	num="":nameid=0:name2id=0:flag=true:alt="":sex=false
	if pc="editSort" then
		sql="select [num],type,type2,flag,alt,sex from kf where id="&id
		tarr=getdbvalue(sql,6)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		num=tarr(1):nameid=clng(tarr(2)):name2id=clng(tarr(3)):flag=tarr(4):alt=tarr(5):sex=tarr(6)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="class_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">帐号号码：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="num" type="text" value="<%=num%>" dataType="LimitB" min="2" max="80" msg="会员名称必须为2-80个字以内">
        <span class="red">*</span></td>
   </tr>
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">提示信息：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="alt" type="text" value="<%=alt%>" size="60" /></td>
   </tr>
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">客服性别：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="sex" type="radio" value="0" <%if not sex then response.Write("checked")%> />男客服&nbsp;<input name="sex" type="radio" value="1"  <%if sex then response.Write("checked")%> />女客服  <span class="red">*</span>  </td>
   </tr>
     <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">帐号类型：</td>
      <td align="left" bgcolor="#FFFFFF">
	  <select name="name" dataType="Require" msg="帐号类型必须选择">
	 <%=getOptionNameList(14,nameid,1)%>
	  </select>
      <span class="red">*</span></td>
   </tr>
     <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">客服类型：</td>
      <td align="left" bgcolor="#FFFFFF">
	  <select name="name2" dataType="Require" msg="客服类型必须选择">
	 <%=getOptionNameList(17,name2id,1)%>
	  </select>
      <span class="red">*</span></td>
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">是否显示：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="flag" type="radio" value="0" <%if not flag then response.Write("checked")%> />不显示&nbsp;<input name="flag" type="radio" value="1"  <%if flag then response.Write("checked")%> />显示    
      <span class="red">*</span></td>
   </tr>     
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		   （<span class="red">*</span>为必填）		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>