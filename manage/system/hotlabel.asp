﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(2,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="hotlabel.asp">网站栏目管理</a></td>
</tr>
</table>
<%
sql="select id,[name],url,target,flag from hotlabel order by adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">栏目名称</td>
	<td align="center">链接地址</td>	
	<td align="center">打开方式</td>	
	<td align="center">当前状态</td>		
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 	
	for i=0 to ubound(tarr,2)
		tempid=tarr(0,i)
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tarr(1,i)%></td>
	<td align="center"><%=tarr(2,i)%></td>
	<td align="center"><%if tarr(3,i) then response.Write("<font color=red>新窗口</font>") else response.Write("<font color=red>当前窗口</font>")%></td>
	<td align="center"><%if tarr(4,i) then response.Write("<font color=green>已显示</font>") else response.Write("<font color=red>已隐藏</font>")%></td>
	<td align="center"><a href="hotlabel.asp?pc=editSort&id=<%=tempid%>">修改</a> | <a href="class_save.asp?id=<%=tempid%>&myAction=sort">排序</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="5" align="right"><a href="hotlabel.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	id=request.QueryString("id")
	name="":url="":target=true:flag=true
	if pc="editSort" then
		sql="select [name],url,target,flag from hotlabel where id="&id
		tarr=getdbvalue(sql,4)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		name=tarr(1):url=tarr(2):target=tarr(3):flag=tarr(4)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="hotlabel_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">关键字名称：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="name" type="text" value="<%=name%>" dataType="LimitB" min="2" max="20" msg="栏目名称必须为2-20个字以内">
        <span class="red">*</span></td>
   </tr>
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">链接地址：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="url" type="text" value="<%=url%>" size="60" ataType="LimitB" min="1" max="100" msg="链接地址必须为2-100个字以内" /><span class="red">*</span></td>
   </tr>
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">打开方式：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="target" type="radio" value="0" <%if not target then response.Write("checked")%> />当前窗口打开&nbsp;<input name="target" type="radio" value="1"  <%if target then response.Write("checked")%> />新窗口打开  <span class="red">*</span>  </td>
   </tr> 
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">是否显示：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="flag" type="radio" value="0" <%if not flag then response.Write("checked")%> />不显示&nbsp;<input name="flag" type="radio" value="1"  <%if flag then response.Write("checked")%> />显示    
      <span class="red">*</span></td>
   </tr>     
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		   （<span class="red">*</span>为必填）		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>