<!--#include file="../top.asp"-->
<!--#include file="../../inc/xmlsub.asp"-->
<!--#include file="../../inc/myxml.asp"-->
<!--#include file="../adminDelSub.asp"-->

<% 
tqxarr=getqx(9,qxarr)
dim myAction,parentID,sortName,num,sortid,id,tarr
dim sortPath,rank,idnum
const xmlFileName="/xml/class.xml" '定义分类xml文件路径
myAction=checkstr(request("myAction"))' 类别操作的动作
parentID=trim(request("parentID")) '选择的类别
sortName=checkstr(request("Name")) '新增的类别名称
num=trim(request("num")) '类别的值
num2=trim(request("num2")) '类别的值
sortid=request("sortid")'类别的排序ID号
flag=request("flag")'是否推荐
ms=checkstr(request("ms"))'描述
id=trim(request("id"))

if not chkrequest(num) then num=0
if not chkrequest(num2) then num2=0
if not chkrequest(sortid) then sortid=0
if not chkNull(ms,1) then ms=left(ms,300)
if flag<>"1" then flag=0 else flag=1
if not chkrequest(parentID) then 
	parentid=0
else
	parentid=clng(parentID)
end if

'================执行操作===============
select case myAction
case "addSort"
	if tqxarr(1)<>1 then showqxmsg
    if not chkRange(sortName,2,50) then
        call showMsg("请输入新增类别的名称(2-50字)。")
    end if
    '--查询父节点的路径
	if parentId<>0 then		
		sql="select id,rank,sortPath from class where ID="&parentID
		tarr=getDBValue(sql,3)
		if ubound(tarr)<1 then		
			call showMsg("指定类别不存在。")
		end if
		sortPath=tarr(3)&","&tarr(1) '父节点的路径
		rank=cint(tarr(2))+1	
		erase tarr
	else
		sortPath="0"
		rank=1
	end if
	sql="insert into [class] (name,parentid,rank,number,number2,sortPath,adddate,total,sortid,flag,ms)values('"&sortName&"',"&parentID&","&rank&","&num&","&num2&",'"&sortpath&"','"&getTime()&"',0,"&sortid&","&flag&",'"&ms&"')"
	'--新增记录	
	id=setDbValue(sql)
	numerr=createNode(parentId,id,sortName)
	if numerr<>0 then
		call showMsg("添加新分类失败，XML操作失败，错误ID："&id&",请联系程序")
	end if		

case "editSort"
	if tqxarr(1)<>1 then showqxmsg
    if not chkRange(sortName,2,50) then
        call showMsg("请输入新增类别的名称(2-50字)。")
    end if
	if not chkrequest(id) then
		call showMsg("ID号丢失")
	end if
    '--更新名称
    sql="update class set Name='"&sortName&"',number="&num&",number2="&num2&",sortid="&sortid&",flag="&flag&",ms='"&ms&"' where ID="&id
    call setDBvalue(sql)
	idnum=editNode(parentId,id,sortName)
	if idnum<>0 then
		call showMsg("编辑分类失败，XML操作失败，错误ID："&id&",请联系程序")
	end if
	
case "deleteSort"  
	if tqxarr(2)<>1 then showqxmsg
    '--删除所有子分类，包括自己。
	if not chkrequest(id) then
		call showMsg("ID号丢失")
	end if
	id=clng(id)
	'if id<4333 then showmsg("系统分类不允许删除，您可以修改此分类，或者添加您需要的分类")
	tarr=getDBvalue("select top 1 rank from class where id="&id,1)
	if tarr(0)=0 then
		call showmsg("找不到此分类")
	end if
	rank=cint(tarr(1))	
	'if rank=1 then
	'	call showMsg("一级分类不允许删除！如有问题请与程序员联系")
	'end if
	call delClass(id)
	idnum=delNode(id)
	if idnum<>0 then
		call showMsg("删除分类失败，XML操作失败，错误ID："&idnum&",请联系程序")
	end if
   
case "sort"
	if tqxarr(1)<>1 then showqxmsg
	if not chkrequest(id) then
		call showMsg("ID号丢失")
	end if
	conn.execute("update class set adddate='"&now()&"' where id="&id)
end select 
call createClassJs(0)
call closeConn()
response.Redirect("class.asp?parentId="&parentId)
%>