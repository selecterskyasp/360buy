﻿<!--#include file="../top.asp"-->
<!--#include file="../../inc/nostylepage.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<title></title>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > 帮助管理</td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
 <tr>
    <td class="tbHeader">编号</td>
	<td class="tbHeader">类别</td>
    <td class="tbHeader">标题</td>
	<td class="tbHeader">来源</td>
	<td class="tbHeader">发布时间</td>
    <td class="tbHeader">操作</td>
  </tr>
<%
	action=request.QueryString("action")
	select case action
		case "del"
			id=request.QueryString("id")
			del_record "help","id="&int(id)&""
			response.Redirect(request.ServerVariables("HTTP_REFERER"))
			response.End()
	end select
	'条件
	sql="id>0"
	'统计字段   
	sql_id="id"  
	'显示字段   
	sql_Field="*"  
	'查询表名   
	sql_table="help"  
	'排序字段   
	sql_order="createdon"  
	'每页记录   
	PageSize=20   
	'获得总数  
	pp="select count("&sql_id&") from "&sql_table&" where "&sql&" "
	'response.Write(pp)
	'response.End()
	set prors=conn.execute(pp)   
	if not prors.eof then   
	recordcount=prors(0)   
	end if   
	prors.close   
	set prors=nothing 
		'总页数  
		if cint(recordcount) = 0 then 
			pagecount=1
		else
			pagecount=Abs(Int(recordcount/PageSize*(-1)))   
		end if
		'获得当前页码   
		if request.querystring("page") = "" then   
			page = 1   
		else   
			page = cint(request("page"))   
			if recordcount< page*PageSize then   
				page=pagecount   
			end if   
		end if   
	'sql语句   
	if page=1 then   
		sql1="SELECT TOP "&PageSize&" "&sql_Field&" from "&sql_table&" where "&sql&" order by "&sql_order&" asc"  
	else   
		sql1="SELECT TOP "&PageSize&" "&sql_Field&" from "&sql_table&" where ("&sql_order&" <(SELECT MIN("&sql_order&") FROM (SELECT TOP "&((Page-1)*PageSize)&" "&sql_order&" FROM "&sql_table&" where "&sql&" order by "&sql_order&" desc) AS tblTMP)) and "&sql&" order by "&sql_order  
	end if 
	set rs=server.CreateObject("adodb.recordset")
	'response.Write sql1
	n=1
	rs.open sql1,conn,1,1
	do while not rs.eof and n<pagesize
	if not rs.eof then 
		set rsv=conn.execute("select top 1 name from class where id="&cint(rs("bid")))
		if not rsv.eof then
			classname=rsv(0) 
		end if
		rsv.close
		set rsv=nothing
%>
  <tr>
    <td class="tbMsg"><%=rs("id")%></td>
	<td class="tbMsg"><%=classname%></td>
    <td class="tbMsg"><%=rs("name")%></td>
    <td class="tbMsg"><%=rs("ly")%></td>
	<td class="tbMsg"><%=rs("createdon")%></td>
	<td class="tbMsg"><a href="list.asp?action=del&id=<%=rs("id")%>">删除</a> | <a href="edit.asp?id=<%=rs("id")%>">修改</a></td>
  </tr>
  <%
  	end if 
	n=n+1
	rs.movenext
	loop
	rs.close
	set rs=nothing
	call closeconn()
  %>
 <tr>
 	<td class="tbFooter" colspan="7"><a href="edit.asp">添加</a></td>
 </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
 <tr>
 	<td class="pagelink" align="right"><%=showpage(pagecount,pagesize,page,recordcount,15)%></td>
 </tr>
</table>
</body>
</html>
