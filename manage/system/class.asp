﻿<!--#include file="../top.asp"-->
<% 
tqxarr=getqx(9,qxarr)
if tqxarr(0)<>1 then showqxmsg
dim parentID,tarr,i
parentID=trim(request.QueryString("parentID")) '选择的类别
if not chkrequest(parentID) then 
	parentid=0
else
	parentid=clng(parentID)
end if

'取导航
if parentID>0 then 
	sql="select name,sortpath from class where id="&parentID
	tarr=getDBvalue(sql,2)
	if ubound(tarr)<1 then
		call showmsg("类别不存在.")
	end if
	dim tsortname,sortpath
	tsortname	= tarr(1)
	sortpath	= tarr(2)
	erase tarr	
	tarr=split(sortpath,",")
	dim nav_title
	if ubound(tarr)>0 then
		nav_title   = " -&gt; <a href=""class.asp?parentId="&parentid
	else
		nav_title   = " -&gt; <a href=""javascript:;"
	end if
	erase tarr
	nav_title=nav_title&""">"&tsortname&"</a>"
	'拆分路径
	
	'取路径名称
	dim ttitle
	sql="select id,name from class where id in("&sortpath&")"
	tarr=getDBvalueList(sql)
	if clng(tarr(0,0))<>0 then
		for i=0 to UBound(tarr,2) 			
			 ttitle= ttitle&" -&gt; <a href='class.asp?parentID="&tarr(0,i)&"'>"&tarr(1,i)&"</a>"
		next
	end if
	erase tarr
	nav_title=ttitle&nav_title
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="class.asp">网站分类管理</a> <%=nav_title%>
	</td>
</tr>
</table>
<%
sql="select id,name,parentid,sortpath,rank,flag from class where parentid="&clng(parentid)&" order by sortid,adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">分类编号</td>
	<td align="center">分类名称</td>
	<td align="center">下级分类数</td>
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 
	dim tempid,nextcount,ttarr,tparentid,prelink
	for i=0 to ubound(tarr,2)
		sortpath=tarr(3,i)
		tempid=tarr(0,i)
		sql="select count(id) from class where parentid="&tempid	
		nextcount=getDbvalue(sql,1)(1)
		if cint(tarr(4,i))>1 then 		
			ttarr=split(sortpath,",")
			if ubound(ttarr)>1 then
			tparentid=ttarr(ubound(ttarr)-1)
			else
			tparentid=0
			end if
			erase ttarr
			prelink=" | <a href=""class.asp?parentid="&tparentid&""">进入上一层</a>"
		end if
%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tempid%></td>
	<td align="center"><%=tarr(1,i)%><%if tarr(5,i) then response.Write("&nbsp;<font color=red>推荐</font>")%></td>
	<td align="center"><%=nextcount%></td>
	<td align="center"><a href="class_save.asp?parentID=<%=tarr(2,i)%>&id=<%=tempid%>&myAction=deleteSort" onclick="return confirm('删除分类将会删除该分类下的所有子分类和所有产品信息不能恢复，确认吗？');">删除</a> | <a href="class.asp?pc=editSort&parentid=<%=tarr(2,i)%>&id=<%=tempid%>">修改</a> | <a href="class.asp?parentid=<%=tempid%>">进入下一层</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="4" align="right"><a href="class.asp?pc=addSort&parentid=<%=parentid%>">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	if tqxarr(1)<>1 then showqxmsg
	id=request.QueryString("id")
	dim sortName,sortid,num,num2,ms,flag
	sortName="":sortid=0:num=0:num2=0:ms="":flag=0
	if pc="editSort" then
		sql="select [name],sortid,number,number2,ms,flag from class where id="&id
		tarr=getdbvalue(sql,6)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		sortName=tarr(1):sortid=tarr(2):num=tarr(3):num2=tarr(4):ms=tarr(5):flag=tarr(6)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="class_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">
		<input type="hidden" name="parentID" value="<%=parentid%>">
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">类别名：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="Name" type="text" value="<%=sortName%>"></td>
   </tr>
     <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">排序ID：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="sortid" type="text" value="<%=sortid%>">(值越大就越靠前)</td>
   </tr>
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">数值1：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="num" type="text" value="<%=num%>"></td>
   </tr> 
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">数值2：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="num2" type="text" value="<%=num2%>"></td>
   </tr> 
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">描叙：</td>
      <td align="left" bgcolor="#FFFFFF"><textarea name="ms" cols="60" rows="3"><%=ms%></textarea>
      (300字以内)</td>
   </tr> 
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">推荐：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="flag" type="radio" value="1" <%if flag then response.Write("checked")%>>
        是
          <input name="flag" type="radio" value="0" <%if not flag then response.Write("checked")%>/>
          否</td>
   </tr>
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
	   <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>