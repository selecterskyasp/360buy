/* jQuery图片预加载插件。  Dev By CssRain.cn */
jQuery.fn.loadthumb = function(options) {
	options = $.extend({
		 src : ""
	},options);
	var _self = this;
	_self.hide();
	var img = new Image();
	$(img).load(function(){
		_self.attr("src", options.src);
		_self.fadeIn("slow");
	}).attr("src", options.src);  //.atte("src",options.src)要放在load后面，
	return _self;
}
//$('#bigimg').loadthumb({src:"images/big_" + (i + 1) + ".jpg"});
